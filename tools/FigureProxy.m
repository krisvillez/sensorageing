function fighan	= FigureProxy (kpiname,dir_data,sensors)  

% -------------------------------------------------------------------------
%  fFigureProxy.m
% -------------------------------------------------------------------------   
% Description
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	fighan	= FigureProxy (kpiname,dir_data,sensors)  
%
% Inputs
%   kpiname     :   Name of measurement
%                   ('Offset', 'SensitivityDecay', or 'SensitivityRise')  
%   dir_data    :   Location of directory with data
%   sensors     :   Sensor description
%                   (column 1: type, column 2: replicate) 
%
% Outputs
%   fighan      :   Handle for the produced figure
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-25

%% Hard-coded meta-data
% 1. Names for the sensor replicates
ReplicateNames	    =   {'a','b'}                       ;
% 2. Names for every individual sensor
typenames={'T1a','T1b','T2a','T2b','T3a','T3b','T4a','T4b',...
            'T5a','T5b','T5c','T5d'}                    ;
typenames           =   typenames(:)                    ;
% 3. String formats
% 3.1. Format for criteria data files
formatCrit          =  [ '%s' repmat('%12.6f',[1 6]) '\n' ]               ;

%% Hard-coded figure tuning
Markers = {'^','^','v'};
% Colours = {'k','w','k'};
Colours = {[215,48,39]/255,[254,224,144]/255,[254,224,144]/255};

%% Get data

% % Automated meta-data
% nReplica        =	length (ReplicateNames)	; 

% Preallocation
nSensor         =   3               ;
Y               =   nan(0,nSensor)	;
names           =   cell(3,1)       ;

% Get measurements - criteria
for iSensor=1:nSensor
    
    iReplica        =   sensors(iSensor,1)  ;
    iType           =   sensors(iSensor,2)  ;
    
    names{iSensor}	=   typenames{(iType-1)*2+iReplica}           ;
    
    filename        =  [	kpiname '_' ReplicateNames{iReplica}	]	;
    filepath_in     =  [	dir_data	filename	'.csv'          ]	;
    data            =       GetData (filepath_in,formatCrit)           ;
    
    n             	=	length(data{iType+1})           ;
    Y(1:n,iSensor)	=   data{iType+1}           ;
    clear data
end

Y   =   Y(1:36,:)   ;

%% Produce figure
fighan         =   figure ('Name','Proxy')	;
if exist ('FigPrep','file'),	FigPrep;	end

for panel = 1:2
    
    switch panel
        case 1
            sensorA	=	1   ;
            sensorB =	2   ;
        case 2
            sensorA =	2   ;
            sensorB =	3   ;
    end
    
    subplot (1,2,panel)
    if exist ('AxisPrep','file'),	AxisPrep;	end
    hold on
    plot (Y(:,sensorA)-Y(:,sensorB),Y(:,sensorA),['k' Markers{sensorA}],'markerfacecolor',Colours{sensorA},'markersize',11)
    plot (Y(:,sensorA)-Y(:,sensorB),Y(:,sensorB),['k' Markers{sensorB}],'markerfacecolor',Colours{sensorB},'markersize',11)
    
    set (gca,'Xlim',[-15 65])
    set (gca,'Xtick',[-10:10:60])
    set (gca,'Ylim',[-85 5])
    set (gca,'Ytick',-80:10:0)
    grid on
    if exist ('PlotHorizontal','file'),	PlotHorizontal(0,'k-'); end
    if exist ('PlotVertical','file'),	PlotVertical(0,'k-');	end
    legend (names([sensorA sensorB]),'Location','East','AutoUpdate','off')
    labelX	=	sprintf('$\\tilde{E}^0_{%s}-\\tilde{E}^0_{%s}$ [mV]',names{sensorA},names{sensorB});
    labelY	=	sprintf('$\\tilde{E}^0$ [mV]');
    xlabel (labelX,'Interpreter','Latex');
    ylabel (labelY,'Interpreter','Latex');
    if panel==2
        set (gca,'Yaxislocation','right')
    end
end
 
end