function [Tmed,Ymed,Ymad] = ExtractMedians (t_phase,t,y,s0,doplot,filename,dir_figures)

% -------------------------------------------------------------------------
%  fExtractMedians.m
% -------------------------------------------------------------------------   
% Description
%   This function computes median values in phase 1, 2, and 3 of sensor
%   characterization test
% -------------------------------------------------------------------------
% Syntax
%
% I/O: [Tmed,Ymed,Ymad] = ...
%           ExtractMedians (t_phase,t,y,s0,doplot,filename,dir_figures)
%
% Inputs
%   t_phase     :   Times of the start of each phase in the test
%   t           :   Times of observations
%   y           :   Observed values 
%   s0          :   Index of phase 0  
%   doplot      :   Plot results (boolean: yes/no) 
%   filename	:   CSV file name to use to name figure
%   dir_figures	:   Directory to store figures in
%
% Outputs
%   Tmed            :   Times of phase starts
%   Ymed            :   Median values
%   Ymad            :   Median absolute deviation with median values
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-02

global InOctave

% Name of phases
medium	=	{'W', 'C4', 'C7', 'C4', 'W' }           ;

% Criterion to accept data 
%dmin	=	+inf (2,1)                              ;
%dmax	=	-inf (2,1)                              ;
d       =   diff (t_phase(s0+[1:3]) )*24*60*60    ;
%dmin	=	min( d, dmin )                          ;

% select the data in the time window of interest (exposure to calibration media)
t_phase_sort	=	sort ( t_phase )                            ;
index           =	find ( t_phase_sort==t_phase(s0+1) )        ;
Tlim            =	t_phase_sort(index+[-2:3])                  ;
include         =	and ( t>=Tlim(1), t<=Tlim(end) )            ;
T               =	t(include)                                  ;
Y               =	y(include,:)                                ;

if doplot
end
drawnow()

% pre-allocation
Tbnd            =	nan (3,2)	;
Tmed            =	nan (3,1)	;
Ymed            =	nan (3,1)	;
Ymad            =	nan (3,1)	;

% for phase 1, 2, and 3, ...
for s=1:3
    tt  =	T-Tlim(2+s)                                     ;
    inc	=	and (tt>=-120/(24*60*60), tt<=-60/(24*60*60))	;
    if s~=1 && d(s-1)<200
        inc	=   [];
    end
    
    % if there are data available
    if ~isempty(inc)
    
        inc             =       find(inc)                   ;
        Tbnd(s,:)       =	[	min(T(inc))	max(T(inc))	]   ;
        med             =       median(Y(inc),1)            ;
        mad             =       median(abs(Y(inc)-med))     ;
        
        mad(med>1000)	=       inf     ;
        med(med>1000)	=       inf     ;
        
        if ~isnan(med) && ~isinf(med)
            absdev      =       abs(Y(inc)-med)                 ;
            minval      =       min(absdev)                     ;
            index       =       find(absdev==minval)            ;
            Tmed(s)     =       T(inc(floor(median(index))))    ;
            Ymed(s)     =       med                             ;
            Ymad(s)     =       mad                             ;
        end
        
    end
    
end

if doplot
    figure('Name', filename ),
    if exist ('FigPrep','file'),	FigPrep;	end
    if exist ('AxisPrep','file'),	AxisPrep;	end
    grid on
    hold on
    plot (T,Y,'k.')
    set (gca,'Xlim',Tlim([1 end]),'Xtick',[floor(Tlim):(1/24/6):ceil(Tlim)])
    xlabel ('Time [HH:MM]','Interpreter','Latex')
    ylabel ('$\tilde{E}$ [mV]','Interpreter','Latex')
    datetick ('x','keeplimits','keepticks')
    if exist ('PlotHorizontal','file')
        PlotHorizontal (0,'k--')        ;
        PlotHorizontal (177,'k--')      ;
    end
    if exist ('PlotVertical','file')
        PlotVertical (Tlim,'k-')        ;
    end
    drawnow ()
    Pos         =	get (gca,'Position')	;
    Ylim        =   get (gca,'Ylim')        ;
    for s=0:4
        % next lines compute the position of the plotted arrow in figure
        % coordinates on the basis of the corresponding axis coordinates
        t0      =	Tlim(s+1)                                   ;
        t1      =	Tlim(s+2)                                   ;
        Xax     =	([ t0 ; t1 ]-Tlim(1))/diff (Tlim([1 end]))	; % x-axis arrow positions
        Yax     =	( diff (Ylim)-5 )/diff (Ylim)*[1 1]         ; % y-axis arrow positions
        Xfig    =	Xax*Pos(3)+Pos(1)                           ; % figure coordinate positions in x direction
        Yfig    =	Yax*Pos(4)+Pos(2)                           ; % figure coordinate positions in y direction
        if InOctave
            annotation ('doublearrow',Xfig,Yfig)                    ;
        else
            annotation ('doublearrow',Xfig,Yfig,'headstyle','plain');
        end
        % add text to arrow
        text (t0+(t1-t0)/2,Ylim(1)+diff(Ylim)*1.03,...
            ['P' num2str(s) ' (' medium{s+1} ')'],...
            'HorizontalAlignment','center',...
            'fontsize',14)
    end
    for s=1:3
        if all (~isnan (Tbnd(s,:)))
            rectangle ('Position',[Tbnd(s,1) Ylim(1)+1 Tbnd(s,2)-Tbnd(s,1) diff(Ylim)-2],'FaceColor',[1 1 1]*.73,'LineStyle','none');
        end
    end
    plot (Tmed,Ymed,'rx','MarkerSize',11,'linewidth',3)
    plot (T,Y,'k.')
    text (T(end),0,' 0 mV','fontsize',14)
    text (T(end),177,' 177 mV','fontsize',14)
    if exist ('PlotHorizontal','file')
        PlotHorizontal (0,'k--');
        PlotHorizontal (177,'k--');
    end
    if exist ('PlotVertical','file')
        PlotVertical (Tlim,'k-');
    end
    drawnow ()
    pause (.1)

    if nargin<7 || isempty (dir_figures)
    else
        % save plot
        ext     =   strfind (filename,'.csv')    ;
        if isempty(ext)
        else
            filepath	=	[	dir_figures	filename(1:(ext-1))	]   ;
            if  ~InOctave
            saveas (gcf,filepath,'fig')
            saveas (gcf,filepath,'epsc')
            saveas (gcf,filepath,'tiff')
            end
        end
    end
end


