function simout = ModelUnivariateGP (ttil,ztil,tsim0,names,showplot,markers)

% -------------------------------------------------------------------------
%  fModelUnivariateGP.m
% -------------------------------------------------------------------------   
% Description
%   This script fits a GP describing an integrated Brownian motion to
%   realizations of this process, with individual parameter sets for every
%   realization.
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	simout = ModelUnivariateGP (ttil,ztil,tsim0,names,showplot,markers)
%
% Inputs
%   ttil        :   Time values organized as cell array of column vectors
%   ztil        :   Observed values organized as cell array of column vectors
%   tsim0       :   Vector of interpolation/extrapolation points
%   names       :   names for the time series
%   showplot    :   Indicates whether plot should be made (boolean)
%   markers     :   Markers used for plotting
%
% Outputs
%   simout      :   simulation results
%
% -------------------------------------------------------------------------
% Model
%
%   The data generating model is:
%       $$ \dot{x} = \xi(t) $$
%       $$ \dot{y} = x(t) $$
%       $$ z_k  = y(t_k) + \zeta_k$$
%	it is defined in $t \in [0,\infty)$ with initial conditions
%       $x(0) = x_o, y(0) = y_o $
%
%	The random variables have the following distributions
%       $$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
%       $$\zeta_k \sim N(0,\sigma^2_{\zeta}) $$
%
%	This model has the following corresponding  GP.
%
%	The state $x(t)$ is white noise,
%       $$x(t) \sim \mathcal{N}(x_0, W_0)$$
%	where $W_0$ corresponds to the Wiener process covariance.
%	This covariance corresponds to _covW(i=0)_ in the gpml package.
%
%	The state $y(t)$ is the 1-time integrated Wiener process,
%       $$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
%	its covariance $W_1$ corresponds to _covW(i=1)_ in the gpml package.
%
%	Finally, the observable $z_k := z(t=t_k)$ is modeled as
%       $$z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:	Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com> 
%% Created: 2018-08-03

%% Gaussian process (GP) model
% The data generating model is:
%
% $$ \dot{x} = \xi(t) $$
% $$ \dot{y} = x(t) $$
% $$ z_k  = y(t_k) + \zeta_k$$
% it is defined in $t \in [0,\infty)$ with initial conditions
% $x(0) = x_o, y(0) = y_o $
%
% The random variables have the following distributions
% $$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
% $$ \zeta_k \sim N(0,\sigma^2_{\zeta}) $$
%
% This model has the following corresponding  GP.
%
% The state $x(t)$ is white noise,
% $$x(t) \sim \mathcal{N}(x_0, W_0)$$
% where $W_0$ corresponds to the Wiener process covariance.
% This covariance corresponds to _covW(i=0)_ in the gpml package.
%
% The state $y(t)$ is the 1-time integrated Wiener process,
% $$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
% its covariance $W_1$ corresponds to _covW(i=1)_ in the gpml package.
%
% Finally, the observable $z_k := z(t=t_k)$ is modeled as
% $$ z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$
%

%% Setup initial values
% initial values for hyper-parameters
hyp_uni = struct ('cov', [], 'mean', [], 'lik', []);

%%
% Linear mean function for the observable
%
meanfunc_uni = {@meanSum, {
    @meanLinear, @meanConst
    }
    };

%%
% The likelihood hyper-parameter is the intensity of the observational noise
% the value is $\log \left(\sigma_\zeta \right)$
%
likfunc_uni = @likGauss;
hyp_uni.lik = 0;

%%
% Covariance function for the continuous observable $z(t)$
% The value of the hyper-parameter is the logarithm  of the standard deviation
% of the 1-time integrated Wiener process.
%
covfunc_uni = {@covW, 1};
hyp_uni.cov = 0;
% The following is equivalent, if we fix the likelihood hyper-parameter to -Inf
%covfunc = {@covSum, {
%                     {@covW, 1}, @covNoise
%                     }
%           }
%hyp.cov = [0; 0];

args_uni  = {@infExact, meanfunc_uni, covfunc_uni, likfunc_uni};


global isoctave
nRow	=	length(ttil);
w       =	2           ; % confidence intervals plotted as w-sigma bands (e.g., 1-sigma, 2-sigma)

%%
% ========================================================================
%	Fit model
% ========================================================================

%%  Fit GP for individual sensors
% Find maximum likelihood parameters
%
% Note: the evaluation is done with _evalc_ because the _minimize_ function
% is too verbose.
for Row=1:nRow
    
    % --------------------------------------------------------------------
    %   Model calibration
    % --------------------------------------------------------------------
    
    fprintf ('---------------------------------------------------------\n')	;
    fprintf (" Univariate GP - Sensor %s\n",names{Row});
    fprintf ('---------------------------------------------------------\n')	;
    
    N = length(ttil{Row}) ;
    
    if N<=0
        fprintf (" No data - Skipping estimation \n");
    else
        polycoeff       =	polyfit (ttil{Row}, ztil{Row}, 1)   ;
        pz              =	polycoeff(:)                        ;
        %zhat{Row}       =	polyval (pz{Row}, ttil{Row})        ;
        
        hyp_init        =	hyp_uni                             ;
        hyp_init.mean	=	pz                                  ;
        
        fprintf (['Optimizing ...\n']);
        if isoctave, fflush (stdout); end
        %hyp             =	minimize (hyp, @gp, -1e3, args_uni{:}, t1, z1);
        st              =	evalc ('hyp_fin = minimize (hyp_init, @gp, -1e3, args_uni{:}, ttil{Row}, ztil{Row});');
        [NLZ DNLZ]      =	gp (hyp_fin, args_uni{:}, ttil{Row}, ztil{Row});
        
        fprintf ('%s', st(end-min(length(st),49):end));
        if isoctave, fflush (stdout); end
        hyp{Row}	=	hyp_fin	;
        
        % Report the quantities of interest
        fprintf (['Estimates: \n']);
        fprintf ("    d_0           = %.6f mV       \n", hyp{Row}.mean(2));
        fprintf ("    r_{d,0}       = %.6f mV/d     \n", hyp{Row}.mean(1));
        fprintf ("    sigma_gamma   = %.6f mV/d^2   \n", exp (hyp{Row}.lik));
        fprintf ("    sigma_eps     = %.9f mV       \n", exp (hyp{Row}.cov));
        if isoctave, fflush (stdout); end
        
    end
end

%%
% ========================================================================
%	Plot results - preparations
% ========================================================================

%
% Generate predictions in an extended interval
% Sample a number of trajectories
%
for Row=1:nRow
    
    N = length(ttil{Row}) ;
    
    if N<=0
    else
        tsim{Row}   =   sort ( [ tsim0(:) ; ttil{Row} ] ) ;
        
        % get mean predictions
        [z dz]      =	gp (hyp{Row}, args_uni{:}, ttil{Row}, ztil{Row}, tsim{Row});
        zhat{Row}   =	z           ;
        yhat{Row}	=	zhat{Row}	;	% zero mean measurement error.
        
        % Add the observation noise covariance to obtain the posterior
        % covariance of the states $y(t)$ and the observations $z(t)$
        %
        [Ky Kp] =	PosteriorCovariance (covfunc_uni, hyp{Row}, ttil{Row}, tsim{Row}, args_uni{2})   ;
        Kpy     =	Kp	+	Ky                                  ;
        Kpz     =	Kpy +	covNoise (hyp{Row}.lik, tsim{Row})	;
        
        dyhat{Row} = diag(Kpy).^(1/2) ;
        dzhat{Row} = diag(Kpz).^(1/2) ;
        
        % Sample the posterior distribution for the state and observations
        nSample     = 11;
        y_samp{Row} = mvnrnd (zhat{Row}, (Kpy+Kpy')/2, nSample).';
        z_samp{Row} = y_samp{Row} + ...
            mvnrnd (zhat{Row}*0, covNoise (hyp{Row}.lik, tsim{Row}), nSample).';
    end
    
end

if showplot
    %%
    % ========================================================================
    %	Plot results with sausage plots
    % ========================================================================
    
    %
    % Plot the GP prediction
    %
    figure , clf
    hold on
    for Row=1:nRow
        if length(ttil{Row})>0
            hz{Row} = ShadowPlot (tsim{Row}, zhat{Row}, w*dzhat{Row} );
        end
    end
    for Row=1:nRow
        if length(ttil{Row})>0
            h(Row) = plot (ttil{Row}, ztil{Row},markers{Row} );
        end
    end
    for Row=1:nRow
        
        if length(ttil{Row})>0
        clr = get (h(Row), 'color');
        set (hz{Row}.line.center, 'color', clr);
        set ([hz{Row}.line.top hz{Row}.line.bottom], 'color', min (clr*0.8, 1), 'linewidth', 1);
        set (hz{Row}.patch, 'facecolor', min (clr*3, 1));
        end
    end
    xlabel ('Time [d]')
    ylabel ('Measurement [mV]')
    title (['GP individual sensors' ])
    legentries = cellstr([ char(names(:)) ]) ;
    legend (h, legentries)
    axis tight
    hold off
    drawnow ()
    
    %%
    % ========================================================================
    %	Plot results with sample trajectories
    % ========================================================================
    
    %
    % Plot posterior mean, 2-sigma CI, and samples from the posterior distribution of $z(t)$
    %
    figure (), clf
    hold on
    for Row=1:nRow
        
        if length(ttil{Row})>0
        plot (tsim{Row},y_samp{Row}, '-', 'color', [0.8 0.8 0.8], 'linewidth', 1);
        end
    end
    for Row=1:nRow
        if length(ttil{Row})>0
        plot (tsim{Row},z_samp{Row}, '.', 'color', [0.8 0.8 0.8], 'linewidth', 1);
        end
    end
    for Row=1:nRow
        if length(ttil{Row})>0
        hy{Row} = plot (tsim{Row}, yhat{Row} + w*[0 1 -1] .* dyhat{Row}, ':', 'linewidth', 1);
        end
    end
    for Row=1:nRow
        if length(ttil{Row})>0
        hz{Row} = plot (tsim{Row}, zhat{Row} + w*[0 1 -1] .* dzhat{Row}, '-', 'linewidth', 1);
        end
    end
    for Row=1:nRow
        if length(ttil{Row})>0
        h(Row)   = plot (ttil{Row}, ztil{Row}, markers{Row});
        clr = get (h(Row), 'color');
        set (hy{Row}(1), 'color', clr);
        set (hy{Row}(2:3), 'color', clr * 0.8);
        set (hy{Row}(2), 'color', get (h(Row), 'color'));
        set (hz{Row}(1), 'color', clr, 'linestyle', ':');
        set (hz{Row}(2:3), 'color', clr * 0.8);
        set (hz{Row}(2), 'color', get (h(Row), 'color'));
        end
    end
    xlabel ('Time [d]')
    ylabel ('Measurement [mV]')
    title (['GP individual sensors' ])
    legentries = cellstr([ char(names(:)) ]) ;
    legend (h, legentries)
    axis tight
    hold off
    drawnow ()
end


% ======================================================================
%	Output
% ======================================================================

simout.tsim = tsim ;
simout.yhat = yhat;
simout.dyhat = dyhat;
simout.zhat = zhat;
simout.dzhat = dzhat;

end