function data = GetData (filepath,format)       

% -------------------------------------------------------------------------
%  fGetData.m
% -------------------------------------------------------------------------   
% Description
%   This script reads data in from CSV files assuming a user-given format
%   and a number of hard-coded but typical options: (1) use NaN for empty
%   cells, (2) apply one headerline, (3) ; as delimiter.
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	data = GetData (filepath,format)       
%
% Inputs
%   filepath	:   Location of data file
%   format      :   Format of data line (see textscan.m for info)
%
% Outputs
%   data        :   cell array with each column in data file as a cell
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-25
%

fileID	=	fopen (filepath,'r')                ;
data    =	textscan (fileID, format, ...
                        'delimiter',';', ...
                        'headerlines',1, ...
                        'emptyvalue', nan)      ;
fclose(fileID)                                  ;

end