function h = ShadowPlot (x,y,ly,uy)

% -*- texinfo -*-
% @deffn {Function File} {@var{h} =} shadowplot (@var{x}, @var{y}, @var{dy})
% @deffnx {Function File} {@var{h} =} shadowplot (@var{x}, @var{y}, @var{ly}, @var{uy})
%  Plot @var{x} vs @var{y} with highlighted band; a.k.a. sausage plot
%
%  The highlighted region is defined by [@var{y}-@var{dy}, @var{y}+@var{dy}]
%  or if given by [@var{y}-@var{ly}, @var{y}+@var{uy}].
%
% The returned structure @var{h} contains handles to all the graphical objects
% created.
%
% @seealso{patch}
% @end deffn

% Copyright (C) 2015 - Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


if nargin<4
    uy=[];
end

  ax = gca();
  if ~ishold (ax)
    cla
  end %if

  idx  = 1:length(x);
  idx1 = [idx idx(end)];
  idx2 = [fliplr(idx) idx(1)];
  X    = x([idx1 idx2]);
  if isempty (uy)
    uy = ly;
  end %if
  lY = (y-ly);
  lY = lY(idx1);
  uY = (y+uy);
  uY = uY(idx2);
  p = patch (X, [lY; uY],[1 0.8 0.8]);
  set (p, 'edgecolor', 'none');

  hold on
  h2 = plot(x, y-ly,'color',[1 0.6 0.6]);
  h3 = plot(x, y+uy,'color',[1 0.6 0.6]);
  h1 = plot(x, y,'-k');
  h  = [h1; h2; h3];
  set (h, 'linewidth', 2)

  if ~ishold (ax)
    hold off
  end %if

  if nargout > 0
    h = struct('line',struct('center',h(1),'bottom',h(2),'top',h(3)),'patch',p);
  end %if

end %function

%!demo
%! t = linspace (0, 2*pi, 100).';
%! y = sin (1.5 * t);
%! dy = 0.3 * cos (4 * t);
%! h = shadowplot (t, y, dy);
%! axis tight
