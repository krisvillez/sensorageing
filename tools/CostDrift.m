function [SSR,betaopt,res,sigma_eps,Mean_ysim,Cov_ysim,Mean_zsim,Cov_zsim] = CostDrift (ttil,ztil,tfault,tsim)

% -------------------------------------------------------------------------
%  fCostDrift.m
% -------------------------------------------------------------------------   
% Description
%   Cost function for excessive drift model
% -------------------------------------------------------------------------
% Syntax
%
% I/O: [SSR,betaopt,res,sigma_eps,Mean_ysim,Cov_ysim,Mean_zsim,Cov_zsim] = 
%           ... CostDrift (ttil,ztil,tfault,tsim)
%
% Inputs
%   ttil        :   Vector of observation times
%   ztil        :   vector of observed values
%   tfault      :   Drift onset time
%   tsim        :   Model simulation times
%
% Outputs
%   SSR         :   Sum of squared residuals
%   betaopt     :   Regression parameters
%   res         :   Residuals
%   sigma_eps	:   MLE standard noise deviation
%   Mean_ysim	:   Expected value for process state ( $y(t)$ )
%   Cov_ysim	:   Expected covariance for process state ( $y(t)$ )
%   Mean_zsim	:   Expected value for measurement ( $z(t)$ )
%   Cov_zsim	:   Expected covariance for measurement ( $z(t)$ )
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-02

%% Points where model needs to be simulated
if nargin<4 || isempty(tsim)
    tsim = ttil ;
end

%% Model identification and simulation
% Linear regression
N	=       length(ztil)                        ;
B	=	[   ones(N,1)	max(0,ttil-tfault)	]   ;
P	=       pinv(B)                             ;

% Evaluate model
betaopt	=	P*ztil          ;
zopt	=	B*betaopt       ;
res     =	zopt-ztil       ;
SSR     =	sum((res).^2)	;

%% Optional outputs
% MLE noise standard deviations
if nargout>=4
    % 
    sigma_eps	=       sqrt (SSR / N)      ;
end
% Covariance matrices for confidence intervals
if nargout>=5
    
    % Covariance for regression parameters (assuming tfault is known)
    Sigma_eps	=       eye (N) * sigma_eps.^2	;
    Cov_beta	=       P*Sigma_eps*P'          ;
    
    % Basis matrix for simulations
    Nsim        =       length (tsim)                           ;
    Bsim        =	[   ones(Nsim,1)	max(0,tsim-tfault)	]	;
    
    % Mean and covariance for simulated process state
    Mean_ysim	=       Bsim*betaopt                            ;
    Cov_ysim	=       Bsim*Cov_beta*Bsim'                     ;
    
    % Mean and covariance for simulated measurements
    Mean_zsim	=       Mean_ysim                               ;
    Cov_zsim	=       Cov_ysim +eye(Nsim)*sigma_eps.^2        ;
    
end


end

