function fighan	= FigureModels (ttil,ztil,simout,names,iPair)

% -------------------------------------------------------------------------
%  fFigureCriteria.m
% -------------------------------------------------------------------------   
% Description
%   This function generates figures showing offset measurements time
%   series, associated model prediction confidence intervals, and
%   prediction error time series
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	fighan	= FigureModels (ttil,ztil,simout,names)
%
% Inputs
%   ttil    :   Time value series as a cell array column vectors
%   ztil	:   Observed value series as a cell array column vectors
%   simout	:   Simulation results as a cell array of structures
%   names   :   Names of the sensors
%
% Outputs
%   fighan	:   Handle for the produced figure
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-25

% --------------------------------------------------------
%   Preliminaries
% --------------------------------------------------------

%% Hard-coded inputs
output = 'brewer'       ;
%output = 'WEFTEC'	;
%output = 'WR'           ;

% Number of models:
nModel      =   length(simout)          ;

% Number of replicas:
nReplica	=	length(simout{1}.tsim)	;

% create figure:
figname     =   sprintf("Models: %s %s",names{1},names{2})  ;
fighan      =	figure ('Name',figname); clf                ;
if exist('FigPrep'), FigPrep (), end

% visualization options
w           =   2   ;   % control width of lines

% --------------------------------------------------------
%   Left panel
% --------------------------------------------------------

% visualization options
markers	=	{'^','v'}               ;
styles	=	{':','-','--'}          ;
switch output
    case 'brewer'
        colours	=	{[44,123,182]/255,[215,25,28]/255} ;
        colourmatrix = true;
    case 'WR'
        colours	=	{[1 1 1]*.11,[1 1 1]*.42} ;
        colourmatrix = false;
    otherwise  
        colours	=	{[0 0.25 1],[1 .25 .0]} ;
        colourmatrix = false;
end

% actual plotting
subplot (1,2,1)
if exist ('AxisPrep'), AxisPrep (), end
hold on

for iReplica=1:nReplica
    %subplot(1,nReplica,iReplica), hold on,
    h(iReplica)	=	plot (ttil{iReplica}, ztil{iReplica}, ['k' markers{iReplica}],'MarkerFaceColor',colours{iReplica},'markersize',11);
    
    for iModel=1:nModel
        tline	=	simout{iModel}.tsim{iReplica}	;
        yhat    =	simout{iModel}.yhat{iReplica}	;
        dyhat	=	simout{iModel}.dyhat{iReplica}	;
        yline	=   yhat + w .* dyhat               ;
        
        hz(iModel,iReplica)	=	plot (tline, yline, styles{iModel}, ...
            'linewidth', iReplica+1, ...
            'color',colours{iReplica});
        
    end
    
    Xlim	=	[	tline(1)	tline(end)	]                           ;
    Xtick	=	[   floor(Xlim(1)/90):ceil(Xlim(end)/90)    ]*90    ;
    
end
for iReplica=1:nReplica
    
    for iModel=1:nModel
        tline	=	simout{iModel}.tsim{iReplica}	;
        yhat	=	simout{iModel}.yhat{iReplica}	;
        dyhat	=	simout{iModel}.dyhat{iReplica}	;
        yline	=   yhat - w .* dyhat               ;
        
        hz1(iModel,iReplica)	=	plot (tline, yline, styles{iModel}, ...
            'linewidth', iReplica+1, ...
            'color',colours{iReplica});
        
    end
    %             tsimd = sort ([0 tfaulthat(iReplica) 660 ]);
    %             Bsim = [ ones(length(tsimd(:)),1)  max(0,tsimd(:)-tfaulthat(iReplica) ) ];
    %             zsim = Bsim * betahat{iReplica} ;
    %             hz1 = plot (tsimd, zsim+ [0 2 -2] .* sigmahat(iReplica), '-', 'linewidth', 1);
    %             hz2 = plot (tsim, zhat{iReplica} + [0 1 -1] .* dzhat{iReplica}, ':', 'linewidth', 1);
    %             hz3 = plot (tsim, zhat_mv{iReplica} + [0 1 -1] .* dzhat_mv{iReplica}, '--', 'linewidth', 1);
    %
    %            % h   = plot (ttil{iReplica}, ztil{iReplica}, 'ko','Color','k','MarkerFaceColor','k','MarkerSize',5);
    %             axis tight
    %             set (gca, 'Ylim', [-55 10], 'Ytick',-50:10:10)
end
for iReplica=1:nReplica
    h(iReplica)	=	plot (ttil{iReplica}, ztil{iReplica}, ['k' markers{iReplica}],'MarkerFaceColor',colours{iReplica},'markersize',11);
    
end

% axes
set (gca,'Xlim',Xlim,'Xtick',Xtick)
grid on

% labels and legend
xlabel (['Time [d]'],'Interpreter','latex' )
ylabel (['${E}^0$ [mV]'],'Interpreter','latex' )
switch nReplica
    case 1
        l	=   legend ([ h(:) ; hz(:,1)] ,{ ...
            [names{1} ': $\tilde{E}^0$'];...
            [names{1} ': $\mu_{1} \pm ' num2str(w) ' \sigma_1$'];...
            [names{1} ': $\mu_{2} \pm ' num2str(w) ' \sigma_2$'];...
            [names{1} ': $\mu_{3} \pm ' num2str(w) ' \sigma_3$'] });
    otherwise
        l	=   legend ([ h(:) ; hz(:,1); hz(:,2) ] ,{ ...
            [names{1} ': $\tilde{E}^0$'];...
            [names{2} ': $\tilde{E}^0$'];
            [names{1} ': $\mu_{1} \pm ' num2str(w) ' \sigma_1$'];...
            [names{1} ': $\mu_{2} \pm ' num2str(w) ' \sigma_2$'];...
            [names{1} ': $\mu_{3} \pm ' num2str(w) ' \sigma_3$'];
            [names{2} ': $\mu_{1} \pm ' num2str(w) ' \sigma_1$'];...
            [names{2} ': $\mu_{2} \pm ' num2str(w) ' \sigma_2$'];...
            [names{2} ': $\mu_{3} \pm ' num2str(w) ' \sigma_3$'] });
end
switch iPair
    case 1
         location = 'NorthWest' ;
    case {2,3}
         location = 'NorthEast' ;
    otherwise
         location = 'SouthWest' ;
end
set (l, 'interpreter', 'latex','location',location);
drawnow ()

% --------------------------------------------------------
%   Right panel
% --------------------------------------------------------

% visualization options
styles	=	{'o','+','x'}           ;

% actual plotting
subplot (1,2,2)
if exist ('AxisPrep'), AxisPrep (), end
hold on

for iReplica=1:nReplica
    for iModel=1:nModel
        tsim = simout{iModel}.tsim{iReplica} ;
        zhat = simout{iModel}.zhat{iReplica} ;
        include = ismember(tsim,ttil{iReplica}) ;
        e = zhat(include)-ztil{iReplica} ;
        he(iModel,iReplica) = plot (ttil{iReplica}, e,styles{iModel}, 'markersize', 11,'color',colours{iReplica},'linewidth',2);
        % ,'linewidth',2
    end
    
end

% axes
set (gca,'Xlim',Xlim,'Xtick',Xtick)
set (gca,'Ylim',[-20 +20])
set (gca,'YAxisLocation','right')
grid on

% labels and legend
xlabel(['Time [d]'],'Interpreter','latex' )
ylabel(['$\mu - \tilde{E}^0$ [mV]'],'Interpreter','latex' )
switch nReplica
    case 1
        l = legend([ he(:,1) ] ,{ ...
            [names{1} ': $\mu_{1} - \tilde{E}^0$'];...
            [names{1} ': $\mu_{2} - \tilde{E}^0$'];...
            [names{1} ': $\mu_{3} - \tilde{E}^0$'] }  );
    otherwise
        l = legend([ he(:,1); he(:,2) ] ,{ ...
            [names{1} ': $\mu_{1} - \tilde{E}^0$'];...
            [names{1} ': $\mu_{2} - \tilde{E}^0$'];...
            [names{1} ': $\mu_{3} - \tilde{E}^0$'];...
            [names{2} ': $\mu_{1} - \tilde{E}^0$'];...
            [names{2} ': $\mu_{2} - \tilde{E}^0$'];...
            [names{2} ': $\mu_{3} - \tilde{E}^0$'] }  );
end
switch iPair
    case 1
         location = 'NorthWest' ;
    case {2,3}
         location = 'South' ;
    otherwise
         location = 'SouthWest' ;
end
set (l, 'interpreter', 'latex','location',location);
drawnow()

end

