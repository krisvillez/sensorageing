function simout = ModelExcessiveDrift (ttil,ztil,tsim0,names,showplot,markers)

% -------------------------------------------------------------------------
%  fModelExcessiveDrift.m
% -------------------------------------------------------------------------   
% Description
%   This script fits a piece-wise linear model describing an excessive
%   drift to individual time series.
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	simout = ModelExcessiveDrift (ttil,ztil,tsim0,names,showplot,markers)
%
% Inputs
%   ttil        :   Time values organized as cell array of column vectors
%   ztil        :   Observed values organized as cell array of column vectors
%   tsim0       :   Vector of interpolation/extrapolation points
%   names       :   names for the time series
%   showplot    :   Indicates whether plot should be made (boolean)
%   markers     :   Markers used for plotting
%
% Outputs
%   simout      :   simulation results
%
% -------------------------------------------------------------------------
% Model
%
%   The data generating model is:
%       $$	x(t)	=	H(t-t_f)            $$
%       $$	\dot{y}	=	r_d . x(t)          $$
%       $$	z_k     =	y(t_k) + \zeta_k	$$
%	It is defined in $t \in [0,\infty)$ with initial conditions
%       $   y(0) = y_o $
%
%   where H(.) is the Heaviside function, t_f the drift onset time, and r_d
%   the drift rate.
%
%	The random variables have the following distributions
%       $$\zeta_k \sim N(0,\sigma^2_{\zeta}) $$
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-07

global InOctave
nRow = length(ttil);

% ========================================================================
%	Set up plot - start with measurements
% ========================================================================

% plotting options
w	=	2	;	% confidence intervals plotted as w-sigma bands (e.g., 1-sigma, 2-sigma)

% initial figure
if showplot
    figure (), clf
    hold on
    for Row=1:nRow
        N = length(ttil{Row}) ;
        if N>0
            h(Row) = plot (ttil{Row}, ztil{Row}, markers{Row});
        end
    end
end

% ========================================================================
%	Fit model and plot results as they are obtained
% ========================================================================

for Row=1:nRow
    
    % --------------------------------------------------------------------
    %   Model calibration
    % --------------------------------------------------------------------
    
    fprintf ('---------------------------------------------------------\n')	;
    fprintf (" Linear drift model - Sensor %s\n",names{Row});
    fprintf ('---------------------------------------------------------\n')	;
    
    N = length(ttil{Row}) ;
    
    if N>0
    
    % grid search
    fprintf (['Optimizing ...\n']);
    fprintf ([' Grid search ...\n']);
    if InOctave, fflush (stdout); else drawnow('update'); end
    SSRbest = inf;
    
    for tfault = min(floor(ttil{Row})):max(floor(ttil{Row}))
        SSRopt = CostDrift(ttil{Row},ztil{Row},tfault) ;
        if SSRopt <= SSRbest
            SSRbest    = SSRopt ;
            tfaultbest = tfault ;
        end
    end
    
    fprintf ([' Fminbnd search ...\n']);
    if InOctave, fflush (stdout); else drawnow('update'); end
    evalc ('tfault_fin = fminbnd(@(tf) CostDrift(ttil{Row},ztil{Row},tf),0,max(floor(ttil{Row})));');
    
    % --------------------------------------------------------------------
    %   Report and store results
    % --------------------------------------------------------------------
    
    tfaulthat(Row)    = tfault_fin ;
    
    tsim{Row} = sort([tsim0 ; ttil{Row} ; tfaulthat(Row) ]);
    [SSR,betaopt,res,sigma_eps,y_,Ky,z_,Kz] = CostDrift (ttil{Row}, ztil{Row}, tfaulthat(Row),tsim{Row});
    betahat{Row}        =	betaopt;
    sigmahat(Row)       =	sigma_eps ;
    Sigma               =	eye (N) * sigmahat(Row).^2 ;
    NegLogLik(Row,1)	=	res(:).' * (Sigma \ res(:)) + log (det (Sigma)) + N * log (pi);
    
    yhat{Row}           =	y_              ;
    dyhat{Row}          =	diag(Ky).^(1/2) ;
    zhat{Row}           =	z_              ;
    dzhat{Row}          =	diag(Kz).^(1/2) ;
    
    % Report the quantities of interest
    fprintf (['Estimates: \n']);
    fprintf ("    d_0       = %.6f mV       \n", betahat{Row}(1));
    fprintf ("    r_d       = %.6f mV/day	\n", betahat{Row}(2));
    fprintf ("    t_f       = %.6f d        \n", tfaulthat(Row));
    fprintf ("    sigma_eps = %.9f mV       \n", sigmahat(Row));
    if InOctave, fflush (stdout); else drawnow('update'); end
    
    % --------------------------------------------------------------------
    %   Plots
    % --------------------------------------------------------------------
    
    if showplot
    hy = plot (tsim{Row}, yhat{Row}+ w*[0 1 -1] .* dyhat{Row}, ':', ...
        'linewidth',1);
    hz = plot (tsim{Row}, zhat{Row}+ w*[0 1 -1] .* dzhat{Row}, '-', ...
        'linewidth',1);
    
    clr = get (h(Row), 'color');
    set (hy(1), 'color', clr,'linestyle',':');
    set (hy(2:3), 'color', clr * 0.8);
    set (hz(1), 'color', clr,'linestyle',':');
    set (hz(2:3), 'color', clr * 0.8);
    end
    
    end
end

%PlotVertical(tfault_fin,'k:');

% ======================================================================
%	Finalizing plot
% ======================================================================

if showplot
    xlabel ('Time [d]')
    ylabel ('Measurement [mV]')
    axis tight
    hold off
    title (['Excessive drift - Individual sensors' ])
    legentries = cellstr (char(names{:})) ;
    legend (h, legentries)
    drawnow ()
end

% ======================================================================
%	Output
% ======================================================================

simout.tsim = tsim ;
simout.yhat = yhat;
simout.dyhat = dyhat;
simout.zhat = zhat;
simout.dzhat = dzhat;

end