function [c dc] = CostMultisensor (hyp, args, tcell, zcell)

% -------------------------------------------------------------------------
%  fCostMultisensor.m
% -------------------------------------------------------------------------   
% Description
%   Wrapper to gp.m to implement multi-task learning with shared covariance
%   hyper-parameters 
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	[c dc] = CostMultisensor (hyp, args, tcell, zcell)
%
% Inputs
%   hyp     :   hyperparameters
%   args	:   Arguments specifying covariance structure
%   tcell   :   Time values organized as cell array of column vectors
%   zcell   :   Observed values organized as cell array of column vectors
%
% Outputs
%   c       :   objective function (cost)
%   dc      :   gradient of the objective function
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:  Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com> 
%% Created:	2018-08-03

assert(length(tcell)==length(zcell),'Error: tcell and zcell not of matching dimensions')

% Initialization
c           =   0                                       ;
dc          =	struct ('mean', [], 'cov', 0, 'lik', 0)	;

% Loop over time series (~ tasks)
nCell       =   length(tcell)	;
for iCell = 1:nCell
    hyp_one         =	hyp                         ;
    % use j-th pair of hyper-params for j-th mean function
    hyp_one.mean	=	hyp.mean((iCell-1)*2+(1:2)) ;
    % use j-th time series to compute cost and derivative
    [c_one,dc_one]	=	gp (hyp_one, args{:}, tcell{iCell}, zcell{iCell});
    % recursive addition
    c       =   c + c_one           ;
    dc.cov	=	dc.cov + dc_one.cov	;
    dc.lik	=	dc.lik + dc_one.lik	;
    % recursive stacking
	dc.mean	=	[   dc.mean	;   dc_one.mean	]   ;
end


end