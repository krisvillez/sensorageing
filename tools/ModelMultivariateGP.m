function simout = ModelMultivariateGP (ttil,ztil,tsim0,names,showplot,markers) 

% -------------------------------------------------------------------------
%  fModelMultivariateGP.m
% -------------------------------------------------------------------------   
% Description
%   This script fits a GP describing an integrated Brownian motion to
%   multiple independent realizations of the same process. 
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	simout = ModelMultivariateGP (ttil,ztil,tsim0,names,showplot,markers) 
%
% Inputs
%   ttil        :   Time values organized as cell array of column vectors
%   ztil        :   Observed values organized as cell array of column vectors
%   tsim0       :   Vector of interpolation/extrapolation points
%   names       :   names for the time series
%   showplot    :   Indicates whether plot should be made (boolean)
%   markers     :   Markers used for plotting
%
% Outputs
%   simout      :   simulation results
%
% -------------------------------------------------------------------------
% Model
%
%   The data generating model is:
%       $$ \dot{x} = \xi(t) $$
%       $$ \dot{y} = x(t) $$
%       $$ z_k  = y(t_k) + \zeta_k$$
%	it is defined in $t \in [0,\infty)$ with initial conditions
%       $x(0) = x_o, y(0) = y_o $
%
%	The random variables have the following distributions
%       $$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
%       $$\zeta_k \sim N(0,\sigma^2_{\zeta}) $$
%
%	This model has the following corresponding  GP.
%
%	The state $x(t)$ is white noise,
%       $$x(t) \sim \mathcal{N}(x_0, W_0)$$
%	where $W_0$ corresponds to the Wiener process covariance.
%	This covariance corresponds to _covW(i=0)_ in the gpml package.
%
%	The state $y(t)$ is the 1-time integrated Wiener process,
%       $$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
%	its covariance $W_1$ corresponds to _covW(i=1)_ in the gpml package.
%
%	Finally, the observable $z_k := z(t=t_k)$ is modeled as
%       $$z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com> 

%% Multi-sensor Gaussian process (GP) model

%% Setup initial values
% initial values for hyper-parameters
hyp_uni = struct ('cov', [], 'mean', [], 'lik', []);

%%
% Linear mean function for the observable
%
meanfunc_uni = {@meanSum, {
    @meanLinear, @meanConst
    }
    };

%%
% The likelihood hyper-parameter is the intensity of the observational noise
% the value is $\log \left(\sigma_\zeta \right)$
%
likfunc_uni = @likGauss;
hyp_uni.lik = 0;

%%
% Covariance function for the continuous observable $z(t)$
% The value of the hyper-parameter is the logarithm  of the standard deviation
% of the 1-time integrated Wiener process.
%
covfunc_uni = {@covW, 1};
hyp_uni.cov = 0;
% The following is equivalent, if we fix the likelihood hyper-parameter to -Inf
%covfunc = {@covSum, {
%                     {@covW, 1}, @covNoise
%                     }
%           }
%hyp.cov = [0; 0];

args_uni  = {@infExact, meanfunc_uni, covfunc_uni, likfunc_uni};

global isoctave
nReplica	=	length(ttil);
w       =	2           ; % confidence intervals plotted as w-sigma bands (e.g., 1-sigma, 2-sigma)


%%
% ========================================================================
%	Fit model
% ========================================================================

%%  Fit GP for multiple sensors
% Find maximum likelihood parameters
%
fprintf ('---------------------------------------------------------\n')	;
fprintf (" Multivariate GP - Sensors %s %s \n",names{1},names{2});
fprintf ('---------------------------------------------------------\n')	;

fprintf ('Optimizing ...\n');
if isoctave, fflush (stdout); end

hyp_init   = hyp_uni ;
pz_stacked = [];
for iReplica=1:nReplica
    N = length(ttil{iReplica}) ;
    if N<=0
        pz_stacked = [ pz_stacked ; nan(2,1) ];
    else
        polycoeff       =	polyfit (ttil{iReplica}, ztil{iReplica}, 1)   ;
        pz              =	polycoeff(:)                        ;
        pz_stacked = [ pz_stacked ; pz ];
    end
end
hyp_init.mean = pz_stacked; % join the hyper-params of the means

NLZ            = CostMultisensor (hyp_init, args_uni, ttil, ztil);

st = evalc ('hyp_fin = minimize (hyp_init, @CostMultisensor, -1e3, args_uni, ttil, ztil);');
fprintf ('%s', st(max(end-49+1,1):end));
if isoctave, fflush (stdout); end

NLZ            = CostMultisensor (hyp_fin, args_uni, ttil, ztil);
NegLogLik(:,3) = NLZ/2 ;
for iReplica=1:nReplica
    hyp_fin.meanrow{iReplica} = hyp_fin.mean((iReplica-1)*2+(1:2));
end
hyp{nReplica+1} = hyp_fin ;

fprintf ("Estimates:\n")
for iReplica=1:nReplica
    fprintf ("	Sensor %s \n",names{iReplica});
    fprintf ("      d_0         = %.9f mV/d \n", hyp_fin.meanrow{iReplica}(1))
    fprintf ("      r_{d,0}     = %.9f mV	\n", hyp_fin.meanrow{iReplica}(2))
end
fprintf ("    sigma_gamma   = %.9f mV/d^2	\n", exp (hyp_fin.lik))
fprintf ("    sigma_eps     = %.9f mV       \n", exp (hyp_fin.cov))
if isoctave, fflush (stdout); end

%%
% ========================================================================
%	Plot results - preparations
% ========================================================================

%%
% Generate predictions in an extended interval
%
for iReplica=1:nReplica
    
    N = length(ttil{iReplica}) ;
    
    if N<=0
    else 
        tsim{iReplica}   =   sort ( [ tsim0(:) ; ttil{iReplica} ] ) ;
        
        hyprow      = hyp_fin;
        hyprow.mean = hyp_fin.meanrow{iReplica};
        
        % get mean predictions
        [z dz]      =	gp (hyprow, args_uni{:}, ttil{iReplica}, ztil{iReplica}, tsim{iReplica});
        zhat{iReplica}   =	z           ;
        yhat{iReplica}	=	zhat{iReplica}	;	% zero mean measurement error.
        
        % Add the observation noise covariance to obtain the posterior
        % covariance of the states $y(t)$ and the observations $z(t)$
        %
        [Ky Kp] =	PosteriorCovariance (covfunc_uni, hyprow, ttil{iReplica}, tsim{iReplica}, args_uni{2})   ;
        Kpy     =	Kp	+	Ky                                  ;
        Kpz     =	Kpy +	covNoise (hyprow.lik, tsim{iReplica})	;
        
        dyhat{iReplica} = diag(Kpy).^(1/2) ;
        dzhat{iReplica} = diag(Kpz).^(1/2) ;
        
        % Sample the posterior distribution for the state and observations
        nSample     = 11;
        y_samp{iReplica} = mvnrnd (zhat{iReplica}, (Kpy+Kpy')/2, nSample).';
        z_samp{iReplica} = y_samp{iReplica} + ...
            mvnrnd (zhat{iReplica}*0, covNoise (hyprow.lik, tsim{iReplica}), nSample).';
        
    end
    
end


if showplot
    %%
    % ========================================================================
    %	Plot results with sausage plots
    % ========================================================================
    
    %
    % Plot the GP prediction
    %
    figure (), clf
    hold on
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        hz{iReplica} = ShadowPlot (tsim{iReplica}, zhat{iReplica}, w*dzhat{iReplica} );
        end
    end
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        h(iReplica) = plot (ttil{iReplica}, ztil{iReplica},markers{iReplica} );
        end
    end
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        clr = get (h(iReplica), 'color');
        set (hz{iReplica}.line.center, 'color', clr);
        set ([hz{iReplica}.line.top hz{iReplica}.line.bottom], 'color', min (clr*0.8, 1), 'linewidth', 1);
        set (hz{iReplica}.patch, 'facecolor', min (clr*3, 1));
        end
    end
    xlabel ('Time [d]')
    ylabel ('Measurement [mV]')
    title (['GP individual sensors' ])
    legentries = cellstr([ char(names(:)) ]) ;
    legend (h, legentries)
    axis tight
    hold off
    drawnow ()
    
    %%
    % ========================================================================
    %	Plot results with sample trajectories
    % ========================================================================
    
    %
    % Plot posterior mean, 2-sigma CI, and samples from the posterior distribution of $z(t)$
    %
    figure (), clf
    hold on
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        plot (tsim{iReplica},y_samp{iReplica}, '-', 'color', [0.8 0.8 0.8], 'linewidth', 1);
        end
    end
    for iReplica=1 %:nReplica
        
        if length(ttil{iReplica})>0
        plot (tsim{iReplica},z_samp{iReplica}, '.', 'color', [0.8 0.8 0.8], 'linewidth', 1);
        end
    end
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        hy{iReplica} = plot (tsim{iReplica}, yhat{iReplica} + w*[0 1 -1] .* dyhat{iReplica}, ':', 'linewidth', 1);
        end
    end
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        hz{iReplica} = plot (tsim{iReplica}, zhat{iReplica} + w*[0 1 -1] .* dzhat{iReplica}, '-', 'linewidth', 1);
        end
    end
    for iReplica=1 %:nReplica
        if length(ttil{iReplica})>0
        h(iReplica)   = plot (ttil{iReplica}, ztil{iReplica}, markers{iReplica});
        clr = get (h(iReplica), 'color');
        set (hy{iReplica}(1), 'color', clr);
        set (hy{iReplica}(2:3), 'color', clr * 0.8);
        set (hy{iReplica}(2), 'color', get (h(iReplica), 'color'));
        set (hz{iReplica}(1), 'color', clr, 'linestyle', ':');
        set (hz{iReplica}(2:3), 'color', clr * 0.8);
        set (hz{iReplica}(2), 'color', get (h(iReplica), 'color'));
        end
    end
    Xlim	=	[	tsim{iReplica}(1)	tsim{iReplica}(end)	]                           ;
    Xtick	=	[   floor(Xlim(1)/90):ceil(Xlim(end)/90)    ]*90    ;
% axes
set (gca,'Xlim',Xlim,'Xtick',Xtick)
grid on
    xlabel ('Time [d]')
    ylabel ('Measurement [mV]')
    title (['GP individual sensors' ])
    legentries = cellstr([ char(names(:)) ]) ;
    legend (h, legentries)
    axis tight
    hold off
    drawnow ()
    
    
% labels and legend
xlabel (['Time [d]'],'Interpreter','latex' )
ylabel (['${E}^0$ [mV]'],'Interpreter','latex' )
drawnow ()

end

% ======================================================================
%	Output
% ======================================================================

simout.tsim = tsim ;
simout.yhat = yhat;
simout.dyhat = dyhat;
simout.zhat = zhat;
simout.dzhat = dzhat;


end
