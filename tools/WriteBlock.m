function WriteBlock (filepath,block,colnames)

% -------------------------------------------------------------------------
%  fWriteBlock.m
% -------------------------------------------------------------------------   
% Description
%   This function writes a block of data into a CSV file assuming:
%   (1) a number of hard-coded but typical options: (i) use NaN for empty
%       cells, (ii) apply one headerline, (iii) use ';' as delimiter. 
%   (2) that the first columns contains timestamps and every next column
%       correponding to numeric measurement values 
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	WriteBlock (filepath,block,colnames)
%
% Inputs
%   filepath	:   Path including folder and name of produced data file
%   block       :   Data matrix with first column being timestamps in
%                   numeric format
%   colnames    :   Names of the variables
%
% Outputs [None]
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-07

%% Organize data
% timestamp data
block_times_num =	block(:,1)                                      ;
% measurements
block_Y         =	block(:,2:end)                                  ;
                    
%% transform timestamps into string format
timeformat      =	'yyyy.mm.dd HH:MM:SS'                           ;
block_times_str	=	datestr (block_times_num,timeformat)            ;

%% Generate data block to write out
% combine data into cell array structure
block_cell      =	[	cellstr(block_times_str)	...
                        num2cell(block_Y)                       ]   ;

%% Generate string formats to write data
% number of measurements
nCol            =       size (block_Y,2)                            ;
% header line format
formatLine0     =       repmat ('%s;',[1 nCol])                     ;
formatLine0     =	[	'%s;'	formatLine0	'\n'               ]	;
% data line format
formatLine1     =       repmat ('%10.6f;',[1 nCol])                 ;
formatLine1     =	[	'%s;'	formatLine1	'\n'               ]	;

%% Actual writing of file
% open file
fileID          =	fopen (filepath,'w')                            ;
% write header
fprintf (fileID, formatLine0, colnames{:} )                         ;
% write data
nRow            =	size (block_cell, 1 )                           ;
for iRow = 1:nRow
    fprintf (fileID, formatLine1, block_cell{iRow,:} )              ;
end
% close file
fclose(fileID)                                                      ;

                    
end

