function fighan = FigureCriteria (kpiname,dir_data)  

% -------------------------------------------------------------------------
%  fFigureCriteria.m
% -------------------------------------------------------------------------   
% Description
%   This function generates figures of the sensor characterization results
%   plotted as time series
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	fighan = FigureCriteria (kpiname,dir_data)  
%
% Inputs
%   kpiname     :   Name of measurement
%                   ('Offset', 'SensitivityDecay', or 'SensitivityRise')  
%   dir_data    :   Location of directory with data
%
% Outputs
%   fighan      :   Handle for the produced figure
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-25

%% Hard-coded inputs
output = 'brewer'       ;
%output = 'WEFTEC'	;
%output = 'WR'           ;

%% Hard-coded meta-data
% 1. Names for the sensor replicates
ReplicateNames	    =   {'a','b'}                       ;
% 2. Names for every individual sensor
typenames={'T1a','T1b','T2a','T2b','T3a','T3b','T4a','T4b',...
            'T5a','T5b','T5c','T5d'}                    ;      
% typenames = [ typenames ; typenames ];
typenames           =   typenames(:)                    ;
% 3. String formats
% 3.1. Format for criteria data files
formatCrit          =  [ '%s' repmat('%12.6f',[1 6]) '\n' ]               ;
% 3.3. Default date/time format
formatDateTime      =   'yyyy.mm.dd HH:MM:SS'           ;

%% Hard-coded figure tuning
Tlim	=	[	0	750                 ]   ;
Ttick	=	[	Tlim(1):60:Tlim(end)	]   ;

switch kpiname
    case {'Offset'}
        switch output
            case 'WR', kpilabel = '$\tilde{E}^0$ [mV]';
            otherwise,  kpilabel = 'Offset [mV]';
        end
        Ylim	=	[   -96	11	]	;
        Ytick	=       -90:10:10	;
        H       =       0           ;
    case {'SensitivityDecay','SensitivityRise'}
        switch kpiname
            case {'SensitivityDecay'}
                switch output
                    case 'WR', kpilabel = '$\tilde{S}_D$ [mV]';
                    otherwise,  kpilabel = 'Sensitivity (decay) [mV/pH]';
                end
            case {'SensitivityRise'}
                switch output
                    case 'WR', kpilabel = '$\tilde{S}_R$ [mV]';
                    otherwise,  kpilabel = 'Sensitivity (rise) [mV/pH]';
                end
        end
        Ylim	=	[   9       111     ]	;   % out-of-focus figure
        Ytick	=       10:10:110           ;   % out-of-focus figure
        H       =       59                  ;
    otherwise
        disp('Unknown variable')
end

switch output
    case 'brewer'
        Markers ={'^','v';'^','v';'^','v';'^','v';'^','v';'^','v'};
        Colours = [ 215,48,39; 252,141,89 ;254,224,144 ; 224,243,248;   69,117,180; 145,191,219]/255 ;
        colourmatrix = true;
    case 'WR'
        Markers ={'^','v';'^','v';'^','v';'o','o';'s','s';'d','d'};
        Colours = {[1 1 1]*0.9,[1 1 1]*.9;[1 1 1]*0.5,[1 1 1]*.5;[1 1 1]*0.1,[1 1 1]*.1;...
                    [1 1 1]*0.7,[1 1 1]*.9;[1 1 1]*0.7,[1 1 1]*.9;[1 1 1]*0.7,[1 1 1]*.9};
                colourmatrix = false;
    otherwise
        Markers ={'^','v';'^','v';'^','v';'o','s';'o','s';'o','s'};
        Colours = {'k';'c';'b';'r';'y';[1 .5 0]};
        Colours = [ Colours  Colours ];        
        colourmatrix = false;
end

%% Get data

% Automated meta-data
nReplica        =	length (ReplicateNames)	; 

% Preallocation
TimeNum         =   nan(0,nReplica)       ;
Measurements	=   nan(0,nReplica,0)     ;

% Get measurements - criteria
for iReplica=1:nReplica
        
    filename    =  [	kpiname '_' ReplicateNames{iReplica}	]	;
    filepath_in =  [	dir_data	filename	'.csv'      ]	;
    data        =       GetData(filepath_in,formatCrit)            ;
    
    % Get timestamps
    TimeStr                 =   data{1}                             ;
    n                       =	length(data{1})                     ;
    TimeNum(1:n,iReplica)	=	datenum (TimeStr, formatDateTime )  ;
                
    nType   	=   length(data)-1                                  ;
    for iType=1:nType
        n                                   =	length(data{iType+1})   ;
        Measurements(1:n,iReplica,iType)    =   data{iType+1}           ;
    end
    clear data
end
nType	=   size(Measurements,3)    ;

% Get measurements - temperature/theoretical sensitivity


% Get key dates
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data

TimeStart       =   keydates_num(1)	;
TimeInstallT5a	=   keydates_num(2)	;
TimeInstallT5b	=   keydates_num(3)	;
TimeInstallT5c	=   keydates_num(4)	;
TimeInstallT5d	=   keydates_num(5)	;

TimeFeedChange1	=   keydates_num(6)	;
TimeFeedChange2	=   keydates_num(7)	;
TimeFeedChange3	=   keydates_num(8)	;
TimeFeedChange4	=   keydates_num(9)	;

%% Produce figure
fighan         =   figure('Name',kpiname)  ;
if exist ('FigPrep','file'),	FigPrep;	end
if exist ('AxisPrep','file'),	AxisPrep;	end
hold on
set(gca, 'Xlim',	Tlim	)
set(gca, 'Xtick',   Ttick	)
set(gca, 'Ylim',	Ylim	)
set(gca, 'Ytick',   Ytick	)
set(gca, 'layer',   'top'   )

POS1	=	[	TimeFeedChange1-TimeStart       ...
                Ylim(1)+.1                      ...
                TimeFeedChange2-TimeFeedChange1	...
                Ylim(2)-Ylim(1)                     ]	;
POS2	=	[	TimeFeedChange3-TimeStart       ...
                Ylim(1)+.1                      ...
                TimeFeedChange4-TimeFeedChange3	...
                Ylim(2)-Ylim(1)                     ]	;
r1 = rectangle ('Position',POS1,'LineStyle','none','FaceColor',[1 1 1]*.73) ;
r2 = rectangle ('Position',POS2,'LineStyle','none','FaceColor',[1 1 1]*.73) ;
for iType = 1:nType
    for iReplica=1:nReplica
        tt	=	TimeNum(:,iReplica)-TimeStart ;
        yy	=	squeeze (Measurements(:,iReplica,iType)) ;
%         inc = find(tt<365);
        %lines((iType-1)*2+iReplica) = plot(tt(inc),yy(inc),...
        if colourmatrix
            colour = Colours(iType,:);
        else
            colour = Colours{iType,iReplica};
        end
        lines((iType-1)*2+iReplica) = plot(tt,yy,...
            ['k' Markers{iType,iReplica}], ...
            'markerfacecolor',colour,...
            'markersize',11);
    end
end
if exist ('PlotHorizontal','file')
    PlotHorizontal (H,'k-') ;
end
if exist ('PlotVertical','file')
    PlotVertical (TimeInstallT5a-TimeStart,'k--','linewidth',2) ;
%     PlotVertical (TimeInstallT5b-TimeStart,'k:','linewidth',2) ;
    PlotVertical (TimeInstallT5c-TimeStart,'k--','linewidth',2) ;
%     PlotVertical (TimeInstallT5d-TimeStart,'k:','linewidth',2) ;
end

legend (lines,typenames,'Location','NorthEastOutside','AutoUpdate','off');
%xlabel ('Time [d]','Interpreter','Latex');
%ylabel (kpilabel,'Interpreter','Latex')
xlabel ('Time [d]');
ylabel (kpilabel)
grid on

end