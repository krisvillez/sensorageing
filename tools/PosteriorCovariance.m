function [K varargout] = PosteriorCovariance (covfunc, hyp, obs, tst, varargin)

%% K = posteriorcov (covfunc, hyp, obs, tst)
% [K Kpar] = posteriorcov (covfunc, hyp, obs, tst, meanfunc)
% Posterior (or conditioned covariance matrices of hidden model $y(t)$
%
% $$ z(t) = y(t) + \zeta, \quad \zeta \sim \mathcal{N(0, \delta(t-t^\prime)\sigma_z^2)}$$
%
% Note: can be computed more efficiently from the structure _post_

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The EMI Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version. 
% 
% The EMI Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:	Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com> 
%% Created: 2018-08-03

if exist ('cholinv','file')
    invert = @cholinv ;
else
    invert = @inv;
end

% This part implements eq. 2.24 in GPML book.
Koo     = feval (covfunc{:}, hyp.cov, obs);
Kto     = feval (covfunc{:}, hyp.cov, tst, obs);
Ktt     = feval (covfunc{:}, hyp.cov, tst);
Koo_inv = invert (Koo + covNoise (hyp.lik, obs));
K       = Ktt - Kto * Koo_inv * Kto.'; % Schur complement
%K       =   (K+K')/2;

% This part implements the second term in the variance in eq. 2.42 in GPML book
if numel(varargin) == 1
    % Compute covariance due to uncertainties in the coefficients of a
    % mean function in a finite dimensional function space
    % m(x) = sum g_i(x) b_i
    meanfunc = varargin{1};
    dimbeta = length (hyp.mean);
    no      = size (obs, 1);
    nt      = size (tst, 1);
    
    % Vandermonde matrix
    Ho = zeros (dimbeta, no);
    Ht = zeros (dimbeta, nt);
    I  = eye (dimbeta); % matrix to select betas
    for i=1:dimbeta
        beta = hyp.mean .* I(:,i);
        Ho(i,:) = feval (meanfunc{:}, beta, obs);
        Ht(i,:) = feval (meanfunc{:}, beta, tst);
    end %for
    R = Ht - Ho * Koo_inv * Kto.';
    % covariance matrix due to parameter uncertainty assuming a vague prior
    Kpar = R.' * invert (Ho * Koo_inv * Ho.') * R;
    varargout{1} = Kpar;
end%if

end

