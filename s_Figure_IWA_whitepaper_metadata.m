%%  Compute criteria
%   This script computes the offsets and sensititivies for every sensor and
%   sensor characterization test 
%% 

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-07


%% Good workspace management
clc
if ~exist('calledby','var')
    clear all
    close all
end

% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')

%% Stage 0. Setup and initialization
fprintf ('=========================================================\n')	;
fprintf (' Setting things up \n')                                       ;
fprintf ('=========================================================\n')	;         
if InOctave, fflush (stdout); else drawnow('update'); end

% specify if plots of individual sensor characterization tests should be generated     
plot_test_graphs	=       false       ;

% Identify folders (modify as necessary)
% 1. Location of meta-data
dir_data            =       '.\data_local\'     ;
% 2. Location to write figures
dir_figures         =       'figures\'          ;

% Hard-coded meta-data:
% 1. Names for the sensor replicates
ReplicateNames	    =       {'a','b'}	;
% 2. Number of potential signals in every flat data file
nType               =       4           ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime      =       'yyyy.mm.dd HH:MM:SS'       ;
% 3.2. Format for encoded laboratory journal
formatJournal       =       repmat ('%s',[1 11])        ;
formatJournal       =	[	formatJournal	'\n'    ]   ;
% 3.3. Format for extracted data
formatTest          =       '%s%12.6f\n'                ;

% Making sure directories exist:
% 1. data folder:
resp	=	sprintf ('Folder %s not available to read and write data files', ...
                        dir_data);
assert (exist (dir_data,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end

% Get key dates
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data

TimeStart           =   keydates_num(1)                         ;
TimeInstallT5a      =   keydates_num(2)                         ;
TimeInstallT5b      =   keydates_num(3)                         ;
TimeInstallT5c      =   keydates_num(4)                         ;
TimeInstallT5d      =   keydates_num(5)                         ;

TimeFeedChange1     =   keydates_num(6)                         ;
TimeFeedChange2     =   keydates_num(7)                         ;
TimeFeedChange3     =   keydates_num(8)                         ;
TimeFeedChange4     =   keydates_num(9)                         ;

% Automated meta-data
nReplica        =	length (ReplicateNames)	;


%% Stage 5. Visualize results
fprintf ('=========================================================\n')	;
fprintf (' Executing step 5: Visualization \n')                         ;
fprintf ('=========================================================\n')	;

% Figures with Y-axes reflecting equal-magnitude effects at pH 6
figtitle        =   'Offset'                            ;
figOffset       =   FigureCriteria_IWA_WhitePaper(figtitle,dir_data)	;
filepath        =	[	dir_figures	figtitle '_IWA_WhitePaper'	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end


fprintf ('=========================================================\n')	; 
fprintf (' Finished \n')                                                ;
fprintf ('=========================================================\n')	;
fprintf ("\n");
if InOctave, fflush (stdout); else drawnow('update'); end