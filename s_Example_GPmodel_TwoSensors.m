%% Example: GP model of multiple sensor measurements
% Assuming two independent realizations of the same process
%
%%

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:	Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com>
%% Created: 2018-07-25

clc
clear all
close all

%% Dependencies
% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')

%% Data and model description
% The next plots show the raw data and a simple linear regression
% The results of the linear regression will be used to initialize the GP model
% in the subsequent sections.
%
filepath	=   [   '.\data_local\Offset_a.csv' ];
data        =       GetData(filepath,'%s%f%f%f%f%f%f')         ;
t1   = datenum (data{1},'yyyy.mm.dd HH:MM:SS');
t1   = t1 - datenum('2016.10.04','yyyy.mm.dd');
z1   = data{2};
N1   = length (z1);
pz1  = polyfit (t1, z1, 1);
y1_  = polyval (pz1, t1);

filepath	=   [   '.\data_local\Offset_b.csv' ];
data        =       GetData(filepath,'%s%f%f%f%f%f%f')         ;
t2   = datenum (data{1},'yyyy.mm.dd HH:MM:SS');
t2   = t2 - datenum('2016.10.04','yyyy.mm.dd');
z2   = data{2};
N2   = length (z2);
pz2  = polyfit (t2, z2, 1);
y2_  = polyval (pz2, t2);

%% Gaussian process (GP) model
% The data generating model is:
%
% $$ \dot{x} = \xi(t) $$
% $$ \dot{y} = x(t) $$
% $$ z_k  = y(t_k) + \zeta_k$$
% it is defined in $t \in [0,\infty)$ with initial conditions
% $x(0) = x_o, y(0) = y_o $
%
% The random variables have the following distributions
% $$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
% $$ \zeta_k \sim N(0,\sigma^2_{\zeta}) $$
%
% This model has the following corresponding  GP.
%
% The state $x(t)$ is white noise,
% $$x(t) \sim \mathcal{N}(x_0, W_0)$$
% where $W_0$ corresponds to the Wiener process covariance.
% This covariance corresponds to _covW(i=0)_ in the gpml package.
%
% The state $y(t)$ is the 1-time integrated Wiener process,
% $$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
% its covariance $W_1$ corresponds to _covW(i=1)_ in the gpml package.
%
% Finally, the observable $z_k := z(t=t_k)$ is modeled as
% $$ z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$
%
% Both data sets should share the same process, except for the initial
% conditions $x_0$ and $y_0$

% initial values for hyper-parameters
hyp10 = struct ('cov', [], 'mean', [], 'lik', []); 
hyp20 = struct ('cov', [], 'mean', [], 'lik', []);

%%
% Linear mean function for the observable
%
meanfunc = {@meanSum, {
                       @meanLinear, @meanConst
                      }
           };
hyp10.mean = pz1(:);
hyp20.mean = pz2(:);

%%
% The likelihood hyper-parameter is the intensity of the observational noise
% the value is $\log \left(\sigma_\zeta \right)$
%
likfunc = @likGauss;
hyp10.lik = 0;
hyp20.lik = 0;

%%
% Covariance function for the continuous observable $z(t)$
% The value of the hyper-parameter is the logarithm  of the standard deviation
% of the 1-time integrated Wiener process.
%
covfunc = {@covW, 1};
hyp10.cov = 0;
hyp20.cov = 0;
% The following is equivalent, if we fix the likelihood hyper-parameter to -Inf
%covfunc = {@covSum, {
%                     {@covW, 1}, @covNoise
%                     }
%           }
%hyp.cov = [0; 0];

args  = {@infExact, meanfunc, covfunc, likfunc};

%%
% Find maximum likelihood parameters
%
% Note: the evaluation is done with _evalc_ because the _minimize_ function
% is too verbose.
if ~exist ('hyp', 'var')
  tcell = {t1, t2};
  zcell = {z1, z2};
  fprintf ('Optimizing ...\n'); 
  if InOctave, fflush (stdout); end
  hyp = hyp10;
  hyp.mean = [hyp10.mean; hyp20.mean]; % join the hyper-params of the means
  st = evalc ('hyp = minimize (hyp, @CostMultisensor, -1e3, args, tcell, zcell);');
  fprintf ('%s', st(end-48:end)); 
  if InOctave, fflush (stdout); end

  % split the means
  hyp1 = hyp;
  hyp2 = hyp;
  hyp1.mean = hyp1.mean(1:2);
  hyp2.mean = hyp2.mean(3:4);
end %if

%%
% Generate predictions in an extended interval
%
t         = linspace (0, max([t1(:); t2(:)])*1.25, 100).';
[z1_ dz2] = gp (hyp1, args{:}, t1, z1, t);
dz1_      = 1.96 * sqrt (dz2);
[z2_ dz2] = gp (hyp2, args{:}, t2, z2, t);
dz2_      = 1.96 * sqrt (dz2);

%%
% Plot the GP prediction
%
figure (1), clf
  hold on
  hz1 = ShadowPlot (t, z1_, dz1_);
  hz2 = ShadowPlot (t, z2_, dz2_);
  h = plot (t1, z1, '^', t2, z2, 'v');
  clr = get (h(1), 'color');
  set (hz1.line.center, 'color', clr);
  set ([hz1.line.top hz1.line.bottom], 'color', min (clr*0.8, 1), 'linewidth', 1);
  set (hz1.patch, 'facecolor', min (clr*3, 1));
  %set (hz1.patch, 'facecolor', clr, 'facealpha', 0.3); % no alpha in html files
  clr = get (h(2), 'color');
  set (hz2.line.center, 'color', clr);
  set ([hz2.line.top hz2.line.bottom], 'color', min (clr*0.8, 1), 'linewidth', 1);
  set (hz2.patch, 'facecolor', min (clr*3, 1));
  %set (hz2.patch, 'facecolor', clr, 'facealpha', 0.3); % no alpha in html files
  xlabel ('Time [d]')
  ylabel ('Measurement [mV]')
  legend (h, {'Run 1','Run 2'})
  axis tight
  hold off

%%
% The estimation of the quantities of interest
%
fprintf ("Run 1:\n")
fprintf ("    x(0) = %.2f mV/day\n", hyp1.mean(1))
fprintf ("    y(0) = %.2f mV\n", hyp1.mean(2))
fprintf ("    std. dev. y(t) = %.2f mV\n", exp (hyp1.cov))
fprintf ("    std. dev. zeta(t) = %.2f mV\n", exp (hyp1.lik))
fprintf ("Run 2:\n")
fprintf ("    x(0) = %.2f mV/day\n", hyp2.mean(1))
fprintf ("    y(0) = %.2f mV\n", hyp2.mean(2))
fprintf ("    std. dev. y(t) = %.2f mV\n", exp (hyp2.cov))
fprintf ("    std. dev. zeta(t) = %.2f mV\n", exp (hyp2.lik))
if InOctave, fflush (stdout); end

%%
% Plots of the residuals, and their autocorrelation functions.
%
res1 = z1 - gp (hyp1, args{:}, t1, z1, t1);
res2 = z2 - gp (hyp2, args{:}, t2, z2, t2);
[acorr1 l1] = xcorr (res1);
[acorr2 l2] = xcorr (res2);

figure (2), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, '^', t2, res2, 'v');
  legend(hres, {'Run 1','Run 2'})
  ylabel('Residual [mV]')
  axis tight

  % Autocorrelation of residuals
  subplot (2, 1, 2)
  hold on
  clr = get (hres(1), 'color');
  stem (l1, acorr1, 'color', clr, 'markerfacecolor', clr);
  clr = get (hres(2), 'color');
  stem (l2, acorr2, 'color', clr, 'markerfacecolor', clr);
  xlabel ('Time [d]')
  axis tight
  hold off

%%
% Add the observation noise covariance to obtain the posterior
% covariance of the observations $z(t)$
%
K1 = PosteriorCovariance (covfunc, hyp1, t1, t) + covNoise (hyp1.lik, t);
K2 = PosteriorCovariance (covfunc, hyp2, t2, t) + covNoise (hyp2.lik, t);

% Sample the posteriors distributions
z1_samp = mvnrnd (z1_, (K1+K1')/2, 25).';
z2_samp = mvnrnd (z2_, (K2+K2')/2, 25).';

%%
% Plot posterior mean, 95% CI, and samples from the posterior distribution
%
figure (3), clf
  hold on
  plot (t, [z1_samp z2_samp], '-', 'color', [0.8 0.8 0.8], 'linewidth', 1);
  hz1 = plot (t, z1_ + [0 1 -1] .* dz1_, '-', 'linewidth', 1);
  hz2 = plot (t, z2_ + [0 1 -1] .* dz2_, '-', 'linewidth', 1);
  h = plot (t1, z1, '^', t2, z2, 'v');
  clr = get (h(1), 'color');
  set (hz1(1), 'color', clr);
  set (hz1(2:3), 'color', clr * 0.8);
  set (hz1(2), 'color', get (h(1), 'color'));
  clr = get (h(2), 'color');
  set (hz2(1), 'color', clr);
  set (hz2(2:3), 'color', clr * 0.8);
  xlabel ('Time [d]')
  ylabel ('Measurement [mV]')
  legend (h, {'Run 1','Run 2'})
  axis tight
  hold off
