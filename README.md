# AGEING PH SENSORS

by 	Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
	Kris Villez <kris.villez@eawag.ch>

Website:	http://www.eawag.ch/en/department/eng/main-focus/sensors-and-automation/ 
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave functions used for data analysis and modeling of data obtained through repeated pH sensor characterization tests.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab R2017b and in Octave 4.2.0 on a Windows system. 

## I. Installing the toolbox and other preliminaries

1. Create a directory to contain all required toolboxes.  For example, in my case all toolbox directories are in: `C:\Tools\`
2. Move the unzipped `Ageing_pH_sensors` folder (in which this `README.md` file is located) to the following location: `C:\Tools\Ageing_pH_sensors\`
3. Download and install the [GPML toolbox](https://gitlab.com/hnickisch/gpml-matlab/) (tested version: gpml-4.2.0) 
4. [Optional] For improved figures, download and install the [Spike_B toolbox](https://gitlab.com/krisvillez/Spike_B_v2.x)
	
## II. Using the toolbox

1. For the purpose of result production, execute the following scripts:
    ```
	script1_compute_criteria
	script2_theoretical_sensitivity
	script3_model_identification
    ```
	The package includes all data to executed these scripts
	
2. To also repeat the extraction of the relevant data before analysis, do the following
	
	2.1 Obtain the original raw data files by contacting Kris Villez <kris.villez@eawag> (Future version will indicate an online repository)

	2.2 Place the obtained data files in a directory `C:\Tools\data_all\`, i.e. in such a way that this package directory (`.\Ageing_pH_sensors\` and the data directory (`.\data_all\`) are located in the same parent directory
	2.3 Execute the scripts
    ```
	script0_data_extraction
	script1_compute_criteria
	script2_theoretical_sensitivity
	script3_model_identification
    ```
	In this case, `script0_data_extraction.m` is used to extract all required data anew.
		
# III. Relevant publications

This toolbox was developed first to produce results in the following proceedings and manuscript:

1.	Ohmura, K, Thürlimann, C M, Kipf, M, Villez, K (2018). Keeping track of pH sensors in biological wastewater treatment systems. Water Environment Federation's 91st Annual Technical Exhibition & Conference (WEFTEC2018), New Orleans, LA, USA, September 29-October 3, 2018, accepted for oral presentation.
2.	Ohmura, K, Thürlimann, C M, Kipf, M, Carbajal, J P, Villez, K (2018). Long-term Wear and Tear of Ion-Selective pH Sensors. In Preparation.
