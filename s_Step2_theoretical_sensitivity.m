%%  Compute criteria
%   This computes the theoretical sensitivity and plots it in figures with
%   the observed sensitivities
%%

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-07


%% Good workspace management
clc
if ~exist('calledby','var')
    clear all
    close all
end

% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')                        ;

%% Setup and initialization
fprintf ('=========================================================\n')	; 
fprintf (' Setting things up \n')                                       ;
fprintf ('=========================================================\n')	; 
if InOctave, fflush (stdout); else drawnow('update'); end

% Identify folders (modify as necessary)
% 1. Location of meta-data
dir_data            =       '.\data_local\'     ;
% 2. Location to write figures
dir_figures         =       'figures\'          ;

% Ensure correct execution of script
% depends on the current directory matching the directory of this script

% location of this script
filepath                =   which(mfilename)        ;
% get directory of this file
[scriptdir,scriptname]	=   fileparts(filepath)     ;
% check if directory matches current directory:
correctlocation         =	strcmp(scriptdir,pwd)   ;
msg                     =   sprintf(...
    'The script %s is not executed in the directory where it is located. Please ensure the current working directory is equal to the location of the script.',...
    scriptname)                                     ;
assert(correctlocation,msg)

%% Hard-coded meta-data:
% 1. Names for the sensor replicates
ReplicateNames	=       {'a','b'}                   ;
% 2. Number of potential signals in every flat data file
nType           =       4                           ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime	=       'yyyy.mm.dd HH:MM:SS'       ;
% 3.2. Format for temperature data files
formatTemp      =		'%s%f\n'                    ;
% 4. Variable names to write data files
varnames        =   {'Timestamp [yyyy.mm.dd HH:MM:SS]','Temperature [Celcius]'};

% Making sure directories exist:
% 1. data folder:
resp	=	sprintf ('Folder %s not available to read and write data files', ...
                        dir_data);
assert (exist (dir_data,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end

%% Temperature data extraction and storage
fprintf ('=========================================================\n')	; 
fprintf (' Get temperatures \n')                                        ;
fprintf ('=========================================================\n')	; 

filemedian	=	[	dir_data	'temperature.csv'           ]	;
data        =       GetData(filemedian,'%s%f')                  ;
Times       =       datenum (data{1} ,'yyyy.mm.dd HH:MM:SS')	;
MedTemp     =       data{2}                                     ;
    
%% Compute theoretical sensitivity
fprintf ('=========================================================\n')	; 
fprintf (' Compute theoretical sensitivity \n')                         ;
fprintf ('=========================================================\n')	; 
if InOctave, fflush (stdout); else drawnow('update'); end

% Constants
R           =	8.3144598           ;	% universal gas constant
F           =	96485.33289         ;	% Faraday constant

% Compute expected sensitivity
TempSlope	=	R/(F*log10(exp(1))) ;	% Theoretical slope [V/pHunit.degK]
TempSlope	=	TempSlope*1000      ;	% Theoretical slope [mV/pHunit.degK]
TempOffset	=	273.15              ;	% 0 Celcius in Kelvin [K]

%calculate theoretical sensitivity by Nernst equation
SensTheory	=	TempSlope*(MedTemp+TempOffset)    ; 

% Get start date
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data
TimeStart       =   keydates_num(1)                         ;

%% Visualization
fprintf ('=========================================================\n')	; 
fprintf (' Visualization \n')                                           ;
fprintf ('=========================================================\n')	; 

% Focused figures for sensitivity
Ylim	=	[   54.5	63.5	]	;   % normal-focus figure
Ytick	=	[   50:1:70         ]	;   % normal-focus figure

% Figure 1
figtitle        =   'SensitivityDecay' ;
figSensDecayF	=   FigureCriteria(figtitle,dir_data)           ;
hans            =   get (figSensDecayF,'Children')              ;
set (hans(end),'Ylim',Ylim,'Ytick',Ytick)                       ;
plot (Times-TimeStart,SensTheory,'w-','LineWidth',5)
plot (Times-TimeStart,SensTheory,'k-','LineWidth',2)
filepath        =	[	dir_figures	figtitle '_theory'	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

% Figure 2
figtitle        =   'SensitivityRise'                           ;
figSensRiseF	=   FigureCriteria(figtitle,dir_data)           ;
hans            =   get (figSensRiseF,'Children')               ;
set (hans(end),'Ylim',Ylim,'Ytick',Ytick)                       ;
plot (Times-TimeStart,SensTheory,'w-','LineWidth',5)
plot (Times-TimeStart,SensTheory,'k-','LineWidth',2)
filepath        =	[	dir_figures	figtitle '_theory'	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

fprintf ('=========================================================\n')	; 
fprintf (' Finished \n')                                                ;
fprintf ('=========================================================\n')	;
fprintf ("\n")                                                          ;
if InOctave, fflush (stdout); else drawnow('update'); end