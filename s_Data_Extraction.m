%%  Data extraction
%   This script extracts all data from the primary data source
%   (repository). This needs only be executed when starting from scratch
%   with the raw data.
%%

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-07


%% Good workspace management
clc
if ~exist('calledby','var')
    clear all
    close all
end

% Display warning message
txt=sprintf ( ...
[   'This script loads all data from the primary data repository for this \n '  ...
    'study. This is only necessary if you want to execute everything from \n ' ...
    'scratch, i.e. starting with the raw data or if the raw data will be  \n ' ...
    'used for a different purpose. \n'  ]);
disp(txt)

% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
addpath('./tools/')

%% Setup and initialization
fprintf ('=========================================================\n')	;
fprintf (' Setting things up \n')                                       ; 
fprintf ('=========================================================\n')	;         
if InOctave, fflush (stdout); else drawnow('update'); end

% specify if plots of individual models should be generated     
plot_individual	=   false               ;

% Identify folders (modify as necessary)
% 1. Location of raw data
dir_primary     =	'..\sensorageing_archive\data_primary\'	;
% 2. Location of processed data
dir_local        =	'.\data_local\'           ;

% Ensure correct execution of script 
% depends on the current directory matching the directory of this script

% location of this script
filepath                =   which(mfilename)        ; 
% get directory of this file  
[scriptdir,scriptname]	=   fileparts(filepath)     ;
% check if directory matches current directory:
correctlocation         =	strcmp(scriptdir,pwd)	;
msg                     =   sprintf(...
    'The script %s is not executed in the directory where it is located. Please ensure the current working directory is equal to the location of the script.',...
    scriptname);
assert(correctlocation,msg)

%% Hard-coded meta-data:
% 1. Names for the sensor replicates
ReplicateNames	=       {'a','b'}                   ;
% 2. Number of potential signals in every flat data file
nType           =       4                           ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime	=   	'yyyy.mm.dd HH:MM:SS'       ;
% 3.2. Format for encoded laboratory journal
formatJournal   =       repmat ('%s',[1 11])        ;
formatJournal	=	[	formatJournal	'\n'    ]   ;
% 3.3. Format for encoded meta-data describing flat data files
formatMeta      =		'%s%s%s\n'                  ;
% 3.4. Format for flat data files with potential measurements
str             =       repmat ('%f',[1 4])         ;
formatPotential	=	[	'%s'	str	'\n'        ]   ;
% 3.5. Format for temperature data files
formatTemp      =       '%s%f\n'                    ;
% 4. Variable names to write data files
varnames        =   {'Timestamp [yyyy.mm.dd HH:MM:SS]','Potential [mV]'};

% Making sure directories exist:
% 1. raw data:
resp	=   sprintf ('Primary data are not available at %s . Please check README.md for instructions.', dir_primary )                               ;
assert (exist (dir_primary,'dir')>0,resp)
% 2. output directory:
resp	=	sprintf ('Making folder %s to write data files', ...
                        dir_local);
if ~exist (dir_local,'dir')
    disp (resp)
    mkdir (pwd,dir_local)
end

% Automated meta-data
nReplica	=	length (ReplicateNames)	;


% Display warning message
txt=sprintf (   'This will take some time.  \n \n Press any key to continue ... \n'  );
disp(txt)
pause


%% Make copies of files needed in dir_local

filename	=   'keydates.csv' ;
[status,message,messageId] = copyfile([dir_primary filename], [dir_local filename]);

filename	=   'nitrogen.csv' ;
[status,message,messageId] = copyfile([dir_primary filename], [dir_local filename]);

% for every set of replicates ...
for iReplica =1:nReplica
    name                =	upper (ReplicateNames{iReplica})            ;
    filename	= [	 'row' name '_testsequence.csv'	]	;
    [status,message,messageId] = copyfile([dir_primary filename], [dir_local filename]);
end
  
%% Data extraction and storage
fprintf ('=========================================================\n')	; 
fprintf (' Get potential data \n')                                      ;
fprintf ('=========================================================\n')	;
if InOctave, fflush (stdout); else drawnow('update'); end

% for every set of replicates ...
for iReplica = 1:nReplica
    
    % Get data from journal
    name                =       upper (ReplicateNames{iReplica})        ;
    filepath_journal	=	[   dir_primary  'row' name '_testsequence.csv' ] ;
    fileIDjournal       =       fopen (filepath_journal,'r')            ;
    data                =       textscan (fileIDjournal, ...
                                            formatJournal, ...
                                            'delimiter',';', ...
                                            'headerlines',1, ...
                                            'emptyvalue', nan)          ;
    
    % Get the timestamps corresponding the start of every sensor
    % characterization test:
    test_time_start	=	datenum (data{2},formatDateTime)            ;
    % Get the timestamps corresponding the end of every sensor
    % characterization test:
    test_time_end	=	datenum (data{end},formatDateTime)          ;
    % clean up
    clear data
    
    % number of listed sensor characterization tests:
    nTest           =	length (test_time_start)                    ;
    
    % Get meta data for online signal files:
    filename        =   [   'row'	name	'_potential_meta.csv'	]           ;
    
    filepath_meta	=	[   dir_primary     filename	]           ;
    data            =       GetData (filepath_meta,formatMeta)      ;
    
    % Get list of flat data file filenames:
    filenames       =	data{1} ;
    % Get first and last stamp in every flat data file
    funempty                    =   @(ca) cellfun(@(c) isempty(c),ca)           ;
    data{2}(funempty(data{2}))	=	{'2000.01.01 00:00:00'}                       ;
    data{3}(funempty(data{3}))	=	{'2000.01.01 00:00:00'}                       ;
    datetimestart               =	datenum (data{2} ,'yyyy.mm.dd HH:MM:SS')    ;
    datetimeend                 =	datenum (data{3} ,'yyyy.mm.dd HH:MM:SS')	;
    clear data
    
    % for every sensor characterization test ...
    for iTest = 1:nTest
        
        % command line output to track progress
        fprintf ('	Replicate %s - Test: %d of %d \n',name,iTest,nTest)                 ;
        if InOctave, fflush (stdout); else drawnow('update'); end
        
        % first time stamp for considered test:
        t_start     =	test_time_start(iTest)                          ;
        % last time stamp for considered test:
        t_end       =	test_time_end(iTest)                            ;
        
        % identify files containing relevant data - These are the files
        % where the first timestamp is before the end of the considered
        % experiment and the final timestamp is after the start of the
        % considered experiment
        condition   =   and (datetimestart<=t_end,t_start<=datetimeend) ;
        fileindex   =   find (condition)                                ;
        
        % initialize data stack
        stack       =   [   ]   ;
        
        % loop over all selected flat data files, get data and add to
        % the stack of data:
        for iFile = fileindex(:)'
            
            % identify file and get data:
            filepath_data	= [	dir_primary	filenames{iFile}	]       ;
            data            =	GetData (filepath_data,formatPotential)	;
            
            % Get time stamps of individual measurements
            timestamp       =   datenum (data{1}, 'yyyy.mm.dd HH:MM:SS');
            
            % Get potential measurements of all sensor type
            measurements    =   cell2mat(data((1:nType)+1))             ;
            
            % Add to stack:
            stack           = [	stack	;	timestamp measurements	]	;
        end
        
        if isempty(stack)
            fprintf ('	No data found for Replicate %s - Test%d\n',...
                ReplicateNames{iReplica},iTest)                         ;
        else
            % select data that are collected between start and end of
            % experiment:
            include =   and (t_start<=stack(:,1),stack(:,1)<=t_end)     ;
            stack   =   stack(include,:)                                ;
            
            for iType=1:nType
                % save data into dedicated file
                % filename
                filepath_out	=	[   dir_local ...
                    'T' num2str(iType) ...
                    ReplicateNames{iReplica} ...
                    '_test' num2str(iTest) ...
                    '.csv'  ]                       ;
                % write data
                WriteBlock (filepath_out, stack(:,[1 iType+1]), varnames)
            end
        end
    end
    
end


if InOctave, fflush (stdout); else drawnow('update'); end


%% Temperature data extraction and storage
fprintf ('=========================================================\n')	; 
fprintf (' Get temperatures \n')                                        ;
fprintf ('=========================================================\n')	;

% file to generate:
filemedian	=	[	dir_local	'temperature.csv'       ]	;
% files to find:
files_in	=	[	dir_primary	'ARA-EawagR5_temperature_day*.csv'	]   ;

DIRLIST_in	=       dir(files_in)       ;
nFile       =       length(DIRLIST_in)  ;

Names       =       {DIRLIST_in.name}'  ;
Names       =       char(Names)         ;

[~,index]	=       sortrows(Names)     ;
DIRLIST_in	=       DIRLIST_in(index)   ;

if nFile>0
    
    % Pre-allocation
    Times       =	nan(nFile,1)        ;
    MedTemp     =	nan(nFile,1)        ;
    
    % For every found file
    for iFile=1:nFile
        disp(['     File: ' num2str(iFile) ' of ' num2str(nFile) ])
        if InOctave, fflush (stdout); else drawnow('update'); end
        
        % Get file contents
        filename        =       DIRLIST_in(iFile).name                  ;
        filepath        =	[	dir_primary     filename	]           ;
        data            =       GetData(filepath,formatTemp)            ;
        
        % Get relevant data
        datetime        =	datenum (data{1} ,'yyyy.mm.dd HH:MM:SS')    ;
        temperature     =   data{2}                                     ;
        
        % If statement needed to run properly in Octave:
        if length(temperature)==1
            Times(iFile)	=	floor((datetime))+.5                    ;
            MedTemp(iFile)	=	(temperature)                           ;
        else
            Times(iFile)	=	floor(nanmedian(datetime))+.5           ;
            MedTemp(iFile)	=	nanmedian(temperature)                  ;
        end
        
    end
    
    % Write data
    varnames = {'Timestamp [yyyy.mm.dd HH:MM:SS]',...
                'Temperature [Celcius]'}                                ;
    WriteBlock(filemedian,[Times MedTemp],varnames)                     ;
    pause (.1) % pausing seems to help to avoid crashes (Octave)
        
end

fprintf ('=========================================================\n')	;
fprintf (' All data extraction finished \n')                            ;
fprintf ('=========================================================\n')	;
fprintf ("\n");
if InOctave, fflush (stdout); else drawnow('update'); end