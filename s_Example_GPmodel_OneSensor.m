%% Example: GP model of sensor measurements
%
%%

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:	Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com>
%% Created: 2018-07-25

clc
clear all
close all

%% Dependencies
% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')

%% Data and model description
% The next plots show the raw data and a simple linear regression
% The results of the linear regression will be used to initialize the GP model
% in the subsequent sections.
%
filepath	=   [   '.\data_local\Offset_a.csv' ];
data        =       GetData(filepath,'%s%f%f%f%f%f%f')         ;
t1   = datenum (data{1},'yyyy.mm.dd HH:MM:SS');
t1   = t1 - datenum('2016.10.04','yyyy.mm.dd');
z1   = data{2};
N1   = length (z1);
pz1  = polyfit (t1, z1, 1);
y1_  = polyval (pz1, t1);

%%
% Plots of the raw data and the linear fit
%
figure ('name','Data'), clf
  hold on
  h = plot (t1, z1, 'o');
  plot (t1, y1_, '-', 'color', get (h(1), 'color'))
  axis tight
  hold off
  xlabel ('Time [d]')
  ylabel ('Measurement [mV]')
  legend (h, {'data'})
  drawnow ()

%%
% Plots of the residuals, and their autocorrelation functions.
%
res1        =	z1 - y1_                ;
[acorr1 l1]	=	xcorr (res1, 'coeff')   ;

figure ('name','Data + Linear trend line'), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, 'o');
  legend ({'data'})
  ylabel ('Residual [mV]')
  axis tight

  % Autocorrelation of residuals
  subplot (2, 1, 2)
  hold on
  clr = get (hres(1), 'color');
  stem (l1, acorr1, 'color', clr, 'markerfacecolor', clr);
  ylabel('Autocorrelation coeff.')
  xlabel('Lag [d]')
  axis tight
  hold off
  drawnow ()

%% Gaussian process (GP) model
% The data generating model is:
%
% $$ \dot{x} = \xi(t) $$
% $$ \dot{y} = x(t) $$
% $$ z_k  = y(t_k) + \zeta_k$$
% it is defined in $t \in [0,\infty)$ with initial conditions
% $x(0) = x_o, y(0) = y_o $
%
% The random variables have the following distributions
% $$\xi(t) \sim \mathcal{N}(0,\delta(t-t^\prime)\sigma_{\xi}^2)$$
% $$ \zeta_k \sim N(0,\sigma^2_{\zeta}) $$
%
% This model has the following corresponding  GP.
%
% The state $x(t)$ is white noise,
% $$x(t) \sim \mathcal{N}(x_0, W_0)$$
% where $W_0$ corresponds to the Wiener process covariance.
% This covariance corresponds to _covW(i=0)_ in the gpml package.
%
% The state $y(t)$ is the 1-time integrated Wiener process,
% $$y(t) \sim \mathcal{N}(x_0 t + y_0, W_1)$$
% its covariance $W_1$ corresponds to _covW(i=1)_ in the gpml package.
%
% Finally, the observable $z_k := z(t=t_k)$ is modeled as
% $$ z(t) \sim \mathcal{N}(x_0 t + y_0, W_1 + \delta(t-t^\prime)\sigma^2_{\zeta}) $$
%

% initial values for hyper-parameters
hyp10 = struct ('cov', [], 'mean', [], 'lik', []);
hyp20 = struct ('cov', [], 'mean', [], 'lik', []);

%%
% Linear mean function for the observable
%
meanfunc = {@meanSum, {
                       @meanLinear, @meanConst
                      }
           };
hyp10.mean = pz1(:);

%%
% The likelihood hyper-parameter is the intensity of the observational noise
% the value is $\log \left(\sigma_\zeta \right)$
%
likfunc = @likGauss;
hyp10.lik = 0;

%%
% Covariance function for the continuous observable $z(t)$
% The value of the hyper-parameter is the logarithm  of the standard deviation
% of the 1-time integrated Wiener process.
%
covfunc = {@covW, 1};
hyp10.cov = 0;
% The following is equivalent, if we fix the likelihood hyper-parameter to -Inf
%covfunc = {@covSum, {
%                     {@covW, 1}, @covNoise
%                     }
%           }
%hyp.cov = [0; 0];

args = {@infExact, meanfunc, covfunc, likfunc};

%%
% Find maximum likelihood parameters
%
% Note: the evaluation is done with _evalc_ because the _minimize_ function
% is too verbose.
if ~exist ('hyp1', 'var')
  fprintf ('Optimizing ...\n'); 
  if InOctave, fflush (stdout); end
  %hyp1  = minimize (hyp10, @gp, -1e3, args{:}, t1, z1);
  st = evalc ('hyp1 = minimize (hyp10, @gp, -1e3, args{:}, t1, z1);');
  fprintf ('%s', st(end-48:end)); 
  if InOctave; fflush (stdout); end
  %nlml1 = gp (hyp1, args{:}, t1, z1);
end %if

%%
% Generate predictions in an extended interval
%
t         = linspace (0, 750, 100).';
[y1_ dy1] = gp (hyp1, args{:}, t1, z1, t);
dy1_      = 1.96 * sqrt (dy1);

%%
% Plot the GP prediction
%
figure ('name','Data + Integrated Brownian motion'), clf
  hold on
  hy1 = ShadowPlot (t, y1_, dy1_);
  h = plot (t1, z1, 'o');
  clr = get (h(1), 'color');
  set (hy1.line.center, 'color', clr);
  set ([hy1.line.top hy1.line.bottom], 'color', min (clr*0.8, 1), 'linewidth', 1);
  set (hy1.patch, 'facecolor', min (clr*3, 1));
  %set (hy1.patch, 'facecolor', clr, 'facealpha', 0.3); % no alpha in html files
  xlabel ('Time [d]')
  ylabel ('Measurement [mV]')
  axis tight
  hold off
  drawnow ()

%%
% The estimation of the quantities of interest
%
fprintf ("MLE parameters:\n");
fprintf ("    x(0) = %.2f mV/day\n", hyp1.mean(1));
fprintf ("    y(0) = %.2f mV\n", hyp1.mean(2));
fprintf ("    std. dev. y(t) = %.2f mV\n", exp (hyp1.cov));
fprintf ("    std. dev. zeta(t) = %.2f mV\n", exp (hyp1.lik));
if InOctave; fflush (stdout); end

%%
% Plots of the residuals, and their autocorrelation functions.
%
res1 = z1 - gp (hyp1, args{:}, t1, z1, t1);
[acorr1 l1] = xcorr (res1);

figure ('name','Integrated Brownian motion residuals'), clf
  subplot (2, 1, 1)
  hres = plot (t1, res1, 'o');
  ylabel ('Residual [mV]')
  axis tight

  % Autocorrelation of residuals
  subplot (2, 1, 2)
  hold on
  clr = get (hres(1), 'color');
  stem (l1, acorr1, 'color', clr, 'markerfacecolor', clr);
  xlabel ('Time [d]')
  axis tight
  hold off
  drawnow ()

%%
% Add the observation noise covariance to obtain the posterior
% covariance of the observations $z(t)$
%
[Ky1 Kpar1] = PosteriorCovariance (covfunc, hyp1, t1, t, meanfunc);
K1  = Ky1 + Kpar1;
K1z = K1 +  covNoise (hyp1.lik, t);

% Sample the posterior distributions of noiseless state
% considering uncrtainty in the mean function parameters
y1_samp = mvnrnd (y1_, (K1+K1')/2, 25).';

%%
% variance of $y(t)$ without parameter uncertainties
vy1_ = diag (Ky1);
dy1_ = 1.96 * sqrt(vy1_);

%%
% variance of $y(t)$ with parameter uncertainties
vy1par_ = diag (K1);
dy1par_ = 1.96 * sqrt(vy1par_);

%%
% variance of $y(t)$ only from parameter uncertainties
v1par_  = diag (Kpar1);
d1par_  = 1.96 * sqrt(v1par_);

%%
% variance of observations $z(t)$ wit parameter uncertainties
vz1par_ = diag (K1z);
dz1par_ = 1.96 * sqrt(vz1par_);

%%
% Plot posterior mean, 95% CI, and samples from the posterior distribution of $z(t)$
%
figure ('name','Data + Integrated Brownian motion'), clf
  hold on
  plot (t, [y1_samp], '-', 'color', [0.8 0.8 0.8], 'linewidth', 1);
  hy1 = plot (t, y1_ + [0 1 -1] .* dy1_, '-', 'linewidth', 1);
  hy1p = plot (t, y1_ + [0 1 -1] .* dy1par_, '--', 'linewidth', 1);
  hy1z = plot (t, y1_ + [0 1 -1] .* dz1par_, ':', 'linewidth', 1);
  h   = plot (t1, z1, 'o');
  clr = get (h(1), 'color');
  set (hy1(1), 'color', clr);
  set (hy1(2:3), 'color', clr * 0.8);
  set (hy1p(1), 'color', clr);
  set (hy1p(2:3), 'color', clr * 0.8);
  set (hy1z(1), 'color', clr);
  set (hy1z(2:3), 'color', clr * 0.8);
  xlabel ('Time [d]')
  ylabel ('Measurement [mV]')
  axis tight
  hold off
  drawnow ()

%%
% Compare variances with and without parameter uncertainty
%
figure ('name','Variance estimates'), clf
%subplot(2,1,1)
  hold on
  h  = plot (t, dy1_, '+');
  hp = plot (t, dy1par_, 'x');
  hz = plot (t, dz1par_, 'o');
  set (hp(1), 'color', get (h(1), 'color'));
  set (hz(1), 'color', get (h(1), 'color'));
  xlabel ('Time [d]')
  ylabel ('Variances [mV^2]')
  legend ({'y','y+par','z+par'})
  axis tight
  hold off
% subplot (2,1,2)
%   hold on
%   h = semilogy (t, v1par_, 'x');
%   axis tight
%   xlabel ('Time [d]')
%   ylabel ('Var. due to param. only [mV^2]')
%  set (gca, 'ygrid', 'on', 'ytick', 10.^[-1:-2:-19])
  drawnow ()
