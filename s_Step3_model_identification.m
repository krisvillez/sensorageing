%%  Identify models and display results
%   This script identifies all models considered to describe the temporal
%   evolution of the recorded offset measurements. 
%% 

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal, Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:	Juan Pablo Carbajal <ajuanpi+dev@gmail.com>, Kris Villez <kris.villez@eawag.com>
%% Created: 2018-07-25

%% Good workspace management
clc
if ~exist('calledby','var')
    clear all
    close all
end

%% Dependencies
% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
dir_gpml = './gpml/';
InOctave            =   LoadDeps (dir_gpml)                ;
addpath('./tools/')

% 1. GPML folder:
resp	=	sprintf ('GPML toolbox not available at %s . Please check README.md for instructions.', ...
                        dir_gpml);
assert (exist (dir_gpml,'dir')>0,resp)

%% Stage 0. Setup and initialization
fprintf ('=============================================== \n')	; 
fprintf (' Setting things up \n')                               ; 
fprintf ('=============================================== \n')	;                 
if InOctave, fflush (stdout); else drawnow('update'); end

warning('off','MATLAB:legend:IgnoringExtraEntries')

% specify if plots of individual sensor characterization tests should be generated     
plot_individual_graphs    =   false	;

% Identify folders (modify as necessary)
% 1. Location of meta-data
dir_data            =       '.\data_local\'     ;
% 2. Location to write figures
dir_figures         =       'figures\'          ;

% Hard-coded meta-data:
% 1. Names for the sensors and replicates
typenames={'T1a','T1b','T2a','T2b','T3a','T3b','T4a','T4b',...
            'T5a','T5b','T5c','T5d'}                    ;
typenames           =   typenames(:)                    ;
ReplicaNames	    =   {'a','b'}                       ;
% 2. Number of potential signals in every flat data file
nReplica            =	length(ReplicaNames)            ;
nPair               =   length(typenames)/nReplica      ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime      =   'yyyy.mm.dd HH:MM:SS'           ;

% Making sure directories exist:
% 1. data folder:
resp	=	sprintf ('Folder %s not available to read and write data files', ...
                        dir_data);
assert (exist (dir_data,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end

% Get key dates
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data

TimeInStall         =   nan(nPair,nReplica)	;
TimeInStall(:,:)    =   keydates_num(1)     ;
TimeInStall(5,1)    =   keydates_num(2)     ;
TimeInStall(5,2)    =   keydates_num(3)     ;
TimeInStall(6,1)    =   keydates_num(4)     ;
TimeInStall(6,2)    =   keydates_num(5)     ;


%% Vizualization options
markers = {'^','v','>','<','o','s','d'} ; % set 7 symbols as default set of markers
chars   = char (96 + (1:7).') ; % set 7 alphabet characters as default set of characters
assert(length(markers)>=nReplica,'Number of rows exceeds number of markers.')
assert(length(chars)>=nReplica,'Number of rows exceeds number of characters.')

for iPair=1:nPair

    fprintf ("=========================================================\n") ;
    fprintf (" Sensor pair: \n",iPair)                                      ;
    fprintf ("=========================================================\n") ;
    
    names   =   typenames((iPair-1)*nReplica+(1:nReplica));

    %% Data and model description
    % The next plots show the raw data and a simple linear regression
    % The results of the linear regression will be used to initialize the GP model
    % in the subsequent sections.
    %
    for iReplica=1:nReplica
        file        = [   'data' num2str(iPair) num2str(iReplica) '.csv' ];
        filepath	= [   dir_data 'Offset_' ReplicaNames{iReplica} '.csv' ];
        
        data	=       GetData(filepath,'%s%f%f%f%f%f%f')          ;
        tt      =       datenum(data{1},formatDateTime)             ;
        zz      =       data{iPair+1}                               ;
        tt      =       tt(~isnan(zz))-TimeInStall(iPair,iReplica)	;
        zz      =       zz(~isnan(zz))                              ;
        
        ttil{iReplica} = tt;
        ztil{iReplica} = zz;
        Ntil(iReplica) = length (ztil{iReplica});
    end
    tmax    =   max(cell2mat(ttil(:))) ;
    tsim	=   (0:ceil(tmax/30+2)*30)';

    %%
    % ===================================================================
    %   ANALYSIS WITH EXCESSIVE DRIFT MODEL
    % ===================================================================
    
    simout{1} = ModelExcessiveDrift(ttil,ztil,tsim,names,...
                                        plot_individual_graphs,markers) ;
    
    %%
    % ===================================================================
    %   ANALYSIS WITH UNIVARIATE GP    
    % ===================================================================
    
    simout{2} = ModelUnivariateGP(ttil,ztil,tsim,names,...
                                        plot_individual_graphs,markers) ;
    
    %%
    % ===================================================================
    %   ANALYSIS WITH UNIVARIATE GP    
    % ===================================================================
    
    simout{3} = ModelMultivariateGP(ttil,ztil,tsim,names,...
                                        plot_individual_graphs,markers)	;
    
    %%
    % ===================================================================
    %   MAKE PLOT WITH ALL RESULTS
    % ===================================================================
    
    fprintf ('---------------------------------------------------------\n')	;
    fprintf (" Visualize all model predictions - Sensors %s %s\n",names{1},names{2});
    fprintf ('---------------------------------------------------------\n')	;
    
    FigureModels(ttil,ztil,simout,names,iPair)    ;
    
    filepath        =	[	dir_figures	'model_pair' num2str(iPair)	]       ;
    drawnow()
    if ~InOctave
        saveas (gcf,filepath,'fig')
        saveas (gcf,filepath,'epsc')
%         saveas (gcf,filepath,'tiff')
    end

    fprintf ("=========================================================\n");
    fprintf ("\n");
    drawnow ()
    pause (.1) % pausing seems to help to avoid crashes (Octave)
    
end

fprintf ('=========================================================\n')	; 
fprintf (' Finished \n')                                                ;
fprintf ('=========================================================\n')	;
fprintf ("\n");
if InOctave, fflush (stdout); else drawnow('update'); end


return
