%%  Breakdown T5b sensors
%   This script produces a figures showcasing the gradual degradation of
%   sensor T5b
%%
% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-12-01

close all
clear all
clc


% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')                        ;

% Identify folders (modify as necessary)
% 1. Location of data
dir_primary        =   '..\sensorageing_archive\data_primary\'	;
% 2. Location to write test criteria results
dir_figures     =   'figures\'	;

% Making sure directories exist:
% 1. raw data:
resp	=   sprintf ('Primary data are not available at %s . Please check README.md for instructions.', dir_primary )                               ;
assert (exist (dir_primary,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end


nType=4;
% 3.4. Format for flat data files with potential measurements
str             =       repmat ('%f',[1 4])         ;
formatPotential	=	[	'%s'	str	'\n'        ]   ;

stackA =[];
stackB =[];

for d=331:365
    
    % identify file and get data:
    filepath_data	= [	dir_primary	'rowA_potential_day' num2str(d,'%03d') '.csv'	]       ;
    
    disp(['Loading: ' filepath_data ])
    data            =	GetData (filepath_data,formatPotential)	;
    
    % Get time stamps of individual measurements
    timestamp       =   datenum (data{1}, 'yyyy.mm.dd HH:MM:SS');
    
    % Get potential measurements of all sensor type
    measurements    =   cell2mat(data((1:nType)+1))             ;
    
    % Add to stack:
    stackA           = [	stackA	;	timestamp measurements	]	;
    
    % identify file and get data:
    filepath_data	= [	dir_primary	'rowB_potential_day' num2str(d,'%03d') '.csv'	]       ;
    data            =	GetData (filepath_data,formatPotential)	;
    
    % Get time stamps of individual measurements
    timestamp       =   datenum (data{1}, 'yyyy.mm.dd HH:MM:SS');
    
    % Get potential measurements of all sensor type
    measurements    =   cell2mat(data((1:nType)+1))             ;
    
    % Add to stack:
    stackB           = [	stackB	;	timestamp measurements	]	;
    
    
end

    disp(['Creating figure ' ])
Tlim = [ datenum('2016.10.04','yyyy.mm.dd') datenum('2018.10.04','yyyy.mm.dd') ];
    
figure, hold on,
if exist ('FigPrep','file'),	FigPrep;	end
if exist ('AxisPrep','file'),	AxisPrep;	end
    plot(stackA(:,1)-Tlim(1),stackA(:,2),'Color',[0 0 1]*.5)
    plot(stackB(:,1)-Tlim(1),stackB(:,2),'Color',[0 0 1])
    plot(stackA(:,1)-Tlim(1),stackA(:,3),'Color',[0 1 0]*.5)
    plot(stackB(:,1)-Tlim(1),stackB(:,3),'Color',[0 1 0]*.73)
    plot(stackA(:,1)-Tlim(1),stackA(:,4),'Color',[1 0 0])
    plot(stackB(:,1)-Tlim(1),stackB(:,4),'Color',[1 0 0]*.73)
    %plot(stackA(:,1)-Tlim(1),stackA(:,5),'Color',[1 1 1]*.42)
    plot(stackB(:,1)-Tlim(1),stackB(:,5),'Color',[1 1 1]*0)
    set(gca,'Ylim',[-200 200],'Ytick',[-200:40:200])
    legend({'T1a','T1b','T2a','T2b','T3a','T3b','T5b'},'Location','SouthWest')
    
    
filepath        =	[	dir_figures	'breakdownT5b'	]       ;
drawnow()

if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end