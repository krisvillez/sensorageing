%%  Complete study
%   This script executed all computations to reproduce the pH sensor ageing
%   study 
%%

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-25

%% Good workspace management
clc
clear all
close all

%% Set calledby to modify behaviour of called scripts
calledby    =   mfilename;

%% Data analyis and modelling
% Compute offset and sensitivity criteria
s_Step1_compute_criteria
% Compute theoretical sensitivity on the basis of temperature
s_Step2_theoretical_sensitivity
% Identify models
s_Step3_model_identification

