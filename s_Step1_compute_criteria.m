%%  Compute criteria
%   This script computes the offsets and sensititivies for every sensor and
%   sensor characterization test 
%% 

% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-08-07


%% Good workspace management
clc
if ~exist('calledby','var')
    clear all
    close all
end

% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')

%% Stage 0. Setup and initialization
fprintf ('=========================================================\n')	;
fprintf (' Setting things up \n')                                       ;
fprintf ('=========================================================\n')	;         
if InOctave, fflush (stdout); else drawnow('update'); end

% specify if plots of individual sensor characterization tests should be generated     
plot_test_graphs	=       false       ;

% Identify folders (modify as necessary)
% 1. Location of meta-data
dir_data            =       '.\data_local\'     ;
% 2. Location to write figures
dir_figures         =       'figures\'          ;

% Hard-coded meta-data:
% 1. Names for the sensor replicates
ReplicateNames	    =       {'a','b'}	;
% 2. Number of potential signals in every flat data file
nType               =       4           ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime      =       'yyyy.mm.dd HH:MM:SS'       ;
% 3.2. Format for encoded laboratory journal
formatJournal       =       repmat ('%s',[1 11])        ;
formatJournal       =	[	formatJournal	'\n'    ]   ;
% 3.3. Format for extracted data
formatTest          =       '%s%12.6f\n'                ;

% Making sure directories exist:
% 1. data folder:
resp	=	sprintf ('Folder %s not available to read and write data files', ...
                        dir_data);
assert (exist (dir_data,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end

% Get key dates
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data

TimeStart           =   keydates_num(1)                         ;
TimeInstallT5a      =   keydates_num(2)                         ;
TimeInstallT5b      =   keydates_num(3)                         ;
TimeInstallT5c      =   keydates_num(4)                         ;
TimeInstallT5d      =   keydates_num(5)                         ;

TimeFeedChange1     =   keydates_num(6)                         ;
TimeFeedChange2     =   keydates_num(7)                         ;
TimeFeedChange3     =   keydates_num(8)                         ;
TimeFeedChange4     =   keydates_num(9)                         ;

% Automated meta-data
nReplica        =	length (ReplicateNames)	;

%% Stage 1. Collect median values 
fprintf ('=========================================================\n')	;
fprintf (' Executing step 1: Collect median values \n')                 ;
fprintf ('=========================================================\n')	;

% Median values are computed in phase 1, 2, and 3 of every sensor characterization test

% pre-allocation
TIME            =   nan(1,nReplica)         ;
MED             =   nan(1,nReplica,3,nType)	;

% for every set of replicates ...
for iReplica =1:nReplica
        
    % Get data from digitized lab journal
    name                =	upper (ReplicateNames{iReplica})            ;
    filepath_journal	= [	dir_data 'row' name '_testsequence.csv'	]	;
    fileIDjournal       =	fopen (filepath_journal,'r')                ;
    data                =	textscan (fileIDjournal, ...
                                            formatJournal, ...
                                            'delimiter',';', ...
                                            'headerlines',1, ...
                                            'emptyvalue', nan)          ;
    fclose (fileIDjournal)	;

    % Get timestamps 
    % of the individual phases in the sensor characterization test
    phase_time              =   nan (length(data{2}),length(data)-1)      ;
    for col=(2:length(data))
        phase_time(:,col-1)   =   datenum (data{col},formatDateTime)      ;
    end
    clear data
        
    %% Number of listed sensor characterization tests:
    nTest                   =	size (phase_time, 1 )                   ;
    
    % Median computation
    % for every sensor type ...
    for iType=1:nType
        
        % command line output to track progress
        fprintf ('---------------------------------------------------------\n')	; 
        fprintf (' Replicate %s - Sensor type: %d of %d \n',name,iType,nType)	;
        fprintf ('---------------------------------------------------------\n')	; 
        if InOctave, fflush (stdout); else drawnow('update'); end
        
        % for every sensor characterization test ...
        for iTest = 1:nTest
            
            % identify file
            filename	=	[   'T' num2str(iType) ReplicateNames{iReplica} ...
                                '_test' num2str(iTest) '.csv'       ]   ;
            filepath_in =	[   dir_data	filename                ]   ;
            
            % get data and compute median values
            if ~exist (filepath_in, 'file' )
                fprintf ('  File %s not found - Skipping \n', filepath_in)               ; 
            else
                data        =	GetData (filepath_in,formatTest)        ;
                
                % Get time series: time (t) and measurements (y)
                t           =	datenum (data{1}, formatDateTime )      ;
                y           =   data{2}                                 ;
                clear data
                
                % Get times of phases in the test
                t_phase     =	phase_time(iTest,:)                     ;
                t_phase     =	t_phase(:)                            ;
                
                % Hard-coded correction for sensor characterization test 8:
                if iTest==8,    s0 = 2  ;
                else,           s0 = 6  ;
                end
                
                % Compute median values
                [Tmed,Ymed,Ymad]	=	...
                    ExtractMedians (t_phase, t, y, s0, ...
                        plot_test_graphs, filename, dir_figures )       ;
                
                    
                % Record the start time of the sensor characterization test
                TIME(iTest,iReplica)        =	t(1)                    ;
                
                % Record the computed median values
                MED(iTest,iReplica,:,iType)	=	Ymed                    ;
                
            end
            
        end

        pause (.1) % pausing seems to help to avoid crashes (Octave)
        
    end
end

%% Stage 2. Re-arrange data 
fprintf ('=========================================================\n')	; 
fprintf (' Executing step 2: Re-arranging data \n')                     ;
fprintf ('=========================================================\n')	;
% such that the data from the 4 installed pairs are distributed across the 6 studied sensor types  

% Expand the matrix to copy last data pair as pair 5 and 6
MED     =	cat (4,MED,MED(:,:,:,4),MED(:,:,:,4))	;
% Overwite data for pair 4 with NaN after replacement with pair 5:
MED(TIME(:,1)>TimeInstallT5a,1,:,4)	=   nan         ;
MED(TIME(:,1)>TimeInstallT5b,2,:,4)	=   nan         ;
% Overwite data for pair 5 with NaN before placement of pair 5 and after replacement with pair 6: 
MED(TIME(:,1)<TimeInstallT5a,1,:,5) =   nan         ;
MED(TIME(:,1)>TimeInstallT5c,1,:,5)	=   nan         ;
MED(TIME(:,1)<TimeInstallT5b,2,:,5) =   nan         ;
MED(TIME(:,1)>TimeInstallT5d,2,:,5)	=   nan         ;
% Overwite data for pair 6 with NaN before placement of pair 6:
MED(TIME(:,1)<TimeInstallT5c,1,:,6)	=   nan         ;
MED(TIME(:,1)<TimeInstallT5d,2,:,6)	=   nan         ;

%% Stage 3. Compute criteria
fprintf ('=========================================================\n')	; 
fprintf (' Executing step 3: Computing criteria \n')                    ;
fprintf ('=========================================================\n')	;
% Compute offset
Offset      =   MED(:,:,2,:)                            ;
% Compute sensitivities
SensDecay	=	(MED(:,:,1,:)-MED(:,:,2,:))/(7.00-4.01) ;
SensRise	=	(MED(:,:,3,:)-MED(:,:,2,:))/(7.00-4.01) ;

%% Stage 4. Store results
fprintf ('=========================================================\n')	; 
fprintf (' Executing step 4: Storing results \n')                       ;
fprintf ('=========================================================\n')	;
for iReplica=1:nReplica
  
    % Get data
    TimeRep         =   squeeze(TIME(:,iReplica))           ;
    OffsetRep       =   squeeze(Offset(:,iReplica,:,:))     ;
    SensDecayRep	=   squeeze(SensDecay(:,iReplica,:,:))	;
    SensRiseRep     =   squeeze(SensRise(:,iReplica,:,:))	;
    
    % Store offset values
    varnames        =   {   'Timestamp [yyyy.mm.dd HH:MM:SS]',...
                            'Offset [mV]'                           }	;
    filepath_out    =	[	dir_data	...
                            'Offset_' ReplicateNames{iReplica} '.csv' ]	;
    WriteBlock (filepath_out, [ TimeRep OffsetRep], varnames)
    
    % Store sensitivity values
    varnames        =   {   'Timestamp [yyyy.mm.dd HH:MM:SS]',...
                            'Sensitivity [mV/pH]'                   }   ;
    filepath_out    =	[	dir_data    ...
                            'SensitivityDecay_' ReplicateNames{iReplica} '.csv'	]   ;  
    WriteBlock (filepath_out, [ TimeRep SensDecayRep], varnames)
    filepath_out    =	[	dir_data    ...
                            'SensitivityRise_' ReplicateNames{iReplica}	'.csv'	]   ;  
    WriteBlock (filepath_out, [ TimeRep SensRiseRep], varnames)
end

%% Stage 5. Visualize results
fprintf ('=========================================================\n')	;
fprintf (' Executing step 5: Visualization \n')                         ;
fprintf ('=========================================================\n')	;

% Figures with Y-axes reflecting equal-magnitude effects at pH 6
figtitle        =   'Offset'                            ;
figOffset       =   FigureCriteria(figtitle,dir_data)	;
filepath        =	[	dir_figures	figtitle	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

figtitle        =   'SensitivityDecay'                  ;
figSensDecay	=   FigureCriteria(figtitle,dir_data)   ;
filepath        =	[	dir_figures	figtitle	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

figtitle        =   'SensitivityRise'                   ;
figSensRise     =   FigureCriteria(figtitle,dir_data)   ;
filepath        =	[	dir_figures	figtitle	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end


% Focused figures for sensitivity
Ylim	=	[   54.5	63.5	]	;   % normal-focus figure
Ytick	=	[   50:1:70         ]	;   % normal-focus figure

figtitle        =   'SensitivityDecay'                  ;
figSensDecayF	=   FigureCriteria(figtitle,dir_data)	;
hans            =   get(figSensDecayF,'Children')                               ;
set (hans(end),'Ylim',Ylim,'Ytick',Ytick)                                         ;
filepath        =	[	dir_figures	figtitle '_focus'	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

figtitle        =   'SensitivityRise'                   ;
figSensRiseF	=   FigureCriteria(figtitle,dir_data)     ;
hans            =   get(figSensRiseF,'Children')                                ;
set (hans(end),'Ylim',Ylim,'Ytick',Ytick)                                         ;
filepath        =	[	dir_figures	figtitle '_focus'	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

% Figure to study potential to use relative differences in offset as a
% proxy for the recorded offsets
figtitle        =   'Offset'                                            ;
figProxy        =   FigureProxy(figtitle,dir_data,[1 1 ; 1 3 ; 2 3])	;
filepath        =	[	dir_figures	figtitle '_proxy'	]       ;
drawnow()
if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end

fprintf ('=========================================================\n')	; 
fprintf (' Finished \n')                                                ;
fprintf ('=========================================================\n')	;
fprintf ("\n");
if InOctave, fflush (stdout); else drawnow('update'); end