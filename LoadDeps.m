function isoctave = LoadDeps (varargin)

% -------------------------------------------------------------------------
%  fLoadDeps.m
% -------------------------------------------------------------------------   
% Description
%   This function loads dependencies in Octave or adds them to the path in
%   Matlab and also produces a boolean indicating whether Octave is used.
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	isoctave = LoadDeps (varargin)
%
% Inputs
%   varargin	:   Packages to load in Matlab [optional, default=[] ]
%
% Outputs
%   isoctave	:   Indicator whether Octave is used (boolean)
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018 Juan Pablo Carbajal
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
%% Created: 2018-08-03


%% Load all dependencies required for scripts
% In Matlab you need to pass the path to all the required packages

persistent loaded
isoctave = exist ('OCTAVE_VERSION', 'builtin');
if isempty (loaded)
    txt = 'Loading dependencies (%s) ...\n';
    if isoctave %if Octave
        % (!) do not use "printf (sprintf ( ) )". Gives error in Matlab
        % even when not executed
        fprintf (txt, 'Octave'); fflush (stdout);
        pkg load signal     % for autocorrelation
        pkg load statistics	% for sampling multivariate normal distribution
        pkg load gpml       % for gp models
    else
        % if not Octave (assumed Matlab)
        fprintf (txt, 'Matlab');
        % for over pkg paths:
        for i=1:nargin
            addpath (genpath (varargin{i}))
        end
    end
    loaded = true;
else
    %already loaded
end

end%function
