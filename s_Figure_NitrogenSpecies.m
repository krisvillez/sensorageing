%%  Nitrogen species concentrations
%   This script produces a figure visualizing the dominant nitrogen species
%   concentration measurements over the course of the long-term experiment.
%%
% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-12-01

close all
clear all
clc


% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')                        ;

% Identify folders (modify as necessary)
% 1. Location of data
dir_data        =   '.\data_local\'	;
% 3. Location to write test criteria results
dir_figures     =   'figures\'	;

% Making sure directories exist:
% 1. data folder:
resp	=	sprintf ('Folder %s not available to read and write data files', ...
                        dir_data);
assert (exist (dir_data,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end

% Hard-coded meta-data:
% 1. Names for the sensors and replicates
typenames={'T1a','T1b','T2a','T2b','T3a','T3b','T4a','T4b',...
            'T5a','T5b','T5c','T5d'}                    ;
typenames           =   typenames(:)                    ;
ReplicaNames	    =   {'a','b'}                       ;
% 2. Number of potential signals in every flat data file
nReplica            =	length(ReplicaNames)            ;
nPair               =   length(typenames)/nReplica      ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime      =   'yyyy.mm.dd HH:MM:SS'           ;


disp(['Loading data ' ])
    

% Get key dates
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data

TimeStart       =   keydates_num(1)	;
TimeInstallT5a	=   keydates_num(2)	;
TimeInstallT5b	=   keydates_num(3)	;
TimeInstallT5c	=   keydates_num(4)	;
TimeInstallT5d	=   keydates_num(5)	;

TimeFeedChange1	=   keydates_num(6)	;
TimeFeedChange2	=   keydates_num(7)	;
TimeFeedChange3	=   keydates_num(8)	;
TimeFeedChange4	=   keydates_num(9)	;

data            =	GetData([dir_data 'nitrogen.csv'],'%s%12.6f%12.6f%12.6f%12.6f%12.6f%12.6f')	;
time_sample     =   data{1}                                 ; % as string
time_sample_num	=   datenum(time_sample,formatDateTime)	; % as numeric

nConc               =   length(data)-1                    ;
concentrations  =   nan(length(time_sample_num),nConc)  ;
for iConc=1:nConc
   	concentrations(:,iConc) = data{iConc+1} ;
end

disp(['Creating figure ' ])
    
Tlim = [ datenum('2016.10.04','yyyy.mm.dd') datenum('2018.10.04','yyyy.mm.dd') ];

figure,
if exist ('FigPrep','file'),	FigPrep;	end

subplot(2,1,1), hold on,
if exist ('AxisPrep','file'),	AxisPrep;	end
marker = {'o','o','o','o','o'};
colour = {[1 0 0]*.5,[1 0 0]*.75,[1 0 0],[0 1 0]*.5,[0 1 0]*.75};
set(gca,'Xlim',Tlim-Tlim(1),'Ylim',[1000 3200],'Ytick',[0:500:3200]);
Ylim = get(gca,'Ylim');
POS1	=	[	TimeFeedChange1-TimeStart       ...
                Ylim(1)+25                      ...
                TimeFeedChange2-TimeFeedChange1	...
                Ylim(2)-Ylim(1)                     ]	;
POS2	=	[	TimeFeedChange3-TimeStart       ...
    Ylim(1)+25                      ...
    TimeFeedChange4-TimeFeedChange3	...
    Ylim(2)-Ylim(1)                     ]	;
r1 = rectangle ('Position',POS1,'LineStyle','none','FaceColor',[1 1 1]*.81) ;
r2 = rectangle ('Position',POS2,'LineStyle','none','FaceColor',[1 1 1]*.81) ;

j=0;
for col = [1:3 5:6 ]
    j=j+1;
    t = time_sample_num-Tlim(1);
    y = concentrations(:,col) ;
    
    plot(t,y,marker{j},'color',colour{j},'markerfacecolor',colour{j},'markersize',3)
    
end

legend({'NH_4^+-N (Dr. Lange)','NH_4^+-N (FIA)','NH_4^+-N (IC)','NO_3^--N (Dr. Lange)','NO_3^--N (IC)'},...
    'AutoUpdate','off','Location','NorthEastOutside')


if exist ('PlotVertical','file')
    PlotVertical (TimeInstallT5a-TimeStart,'k--','linewidth',2) ;
    %     PlotVertical (TimeInstallT5b-TimeStart,'k:','linewidth',2) ;
    PlotVertical (TimeInstallT5c-TimeStart,'k--','linewidth',2) ;
    %     PlotVertical (TimeInstallT5d-TimeStart,'k:','linewidth',2) ;
end
h=ylabel({'Nitrogen concentration','[mgN/L]'});
Pos =get(h,'Position');
Pos(1) = -73 ;
set(h,'Position',Pos)
set(gca,'Xticklabel',[])

subplot(2,1,2), hold on,
colour = {[0 0 1]*.73};
if exist ('AxisPrep','file'),	AxisPrep;	end
marker = {'k.'};
set(gca,'Xlim',Tlim-Tlim(1),'Ylim',[0 100],'Ytick',[0:20:100]);
Ylim = get(gca,'Ylim');
POS1	=	[	TimeFeedChange1-TimeStart       ...
    Ylim(1)+2.5                     ...
    TimeFeedChange2-TimeFeedChange1	...
    Ylim(2)-Ylim(1)                     ]	;
POS2	=	[	TimeFeedChange3-TimeStart       ...
    Ylim(1)+2.5                      ...
    TimeFeedChange4-TimeFeedChange3	...
    Ylim(2)-Ylim(1)                     ]	;
r1 = rectangle ('Position',POS1,'LineStyle','none','FaceColor',[1 1 1]*.81) ;
r2 = rectangle ('Position',POS2,'LineStyle','none','FaceColor',[1 1 1]*.81) ;
j=0;
for col = [4  ]
    j=j+1;
    
    t = time_sample_num-Tlim(1);
    y = concentrations(:,col) ;
    
    plot(t,y,'o','color',colour{j},'markerfacecolor',colour{j},'markersize',3)
    
end
if exist ('PlotVertical','file')
    PlotVertical (TimeInstallT5a-TimeStart,'k--','linewidth',2) ;
    %     PlotVertical (TimeInstallT5b-TimeStart,'k:','linewidth',2) ;
    PlotVertical (TimeInstallT5c-TimeStart,'k--','linewidth',2) ;
    %     PlotVertical (TimeInstallT5d-TimeStart,'k:','linewidth',2) ;
end
xlabel('Time [d]')
h=ylabel({'Nitrogen concentration','[mgN/L]'});
Pos =get(h,'Position');
Pos(1) = -73 ;
set(h,'Position',Pos)

legend({'NO_2^--N (Dr. Lange)'},...
    'AutoUpdate','off','Location','NorthEastOutside')

filepath        =	[	dir_figures	'nitrogen'	]       ;
drawnow()

if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end