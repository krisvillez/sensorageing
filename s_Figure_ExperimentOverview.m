%%  Overview of exposure experiment
%   This script produces a figure visualizing the exposure of the sensors
%   during the long-term experiment
%%
% -------------------------------------------------------------------------
% Copyright 2018 Kris Villez
%
% This file is part of the sensorageing package. 
% 
% The sensorageing package is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version. 
% 
% The sensorageing package is distributed in the hope that it will be
% useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the sensorageing package. If not, see
% <http://www.gnu.org/licenses/>.   
% -------------------------------------------------------------------------

%% Author:   Kris Villez <kris.villez@eawag.com>
%% Created:  2018-12-01

close all
clear all
clc


% Check if this is run in Octave, otherwise Matlab assumed
global InOctave
InOctave            =   LoadDeps ('./gpml/')                ;
addpath('./tools/')                        ;

% Identify folders (modify as necessary)
% 1. Location of data
dir_data        =   '.\data_local\'	;
% 3. Location to write test criteria results
dir_figures     =   'figures\'	;

% Making sure directories exist:
% 1. data folder:
resp	=	sprintf ('Folder %s not available to read and write data files', ...
                        dir_data);
assert (exist (dir_data,'dir')>0,resp)
% 2. figure directory:
if ~exist (dir_figures,'dir')
    mkdir (pwd,dir_figures)
end

% Hard-coded meta-data:
% 1. Names for the sensors and replicates
typenames={'T1a','T1b','T2a','T2b','T3a','T3b','T4a','T4b',...
            'T5a','T5b','T5c','T5d'}                    ;
typenames           =   typenames(:)                    ;
ReplicaNames	    =   {'a','b'}                       ;
% 2. Number of potential signals in every flat data file
nReplica            =	length(ReplicaNames)            ;
nPair               =   length(typenames)/nReplica      ;
% 3. String formats
% 3.1. Default date/time format
formatDateTime      =   'yyyy.mm.dd HH:MM:SS'           ;

disp(['Loading data ' ])
    

% Get key dates
data            =	GetData([dir_data 'keydates.csv'],'%s%s')	;
keydates_str    =   data{1}                                 ; % as string
keydates_num	=   datenum(keydates_str,formatDateTime)	; % as numeric
clear data


TimeInStall         =   nan(nPair,nReplica)	;
TimeInStall(:,:)    =   keydates_num(1)     ;
TimeInStall(5,1)    =   keydates_num(2)     ;
TimeInStall(5,2)    =   keydates_num(3)     ;
TimeInStall(6,1)    =   keydates_num(4)     ;
TimeInStall(6,2)    =   keydates_num(5)     ;
TimeEnd=datenum('2018.10.04 00:00:00',formatDateTime) ;

fighan         =   figure('Name','ExperimentOverview')  ;
for j=1:6
    rectangle('Position',[0 -j-.37 TimeEnd-TimeInStall(1,1) .73],'faceColor','k')
end
j=7;
    rectangle('Position',[0 -j-.37 TimeInStall(5,1)-TimeInStall(1,1) .73],'faceColor','k')
j=8;
    rectangle('Position',[0 -j-.37 TimeInStall(5,2)-TimeInStall(1,1) .73],'faceColor','k')
j=9;
    rectangle('Position',[TimeInStall(5,1)-TimeInStall(1,1) -j-.37 TimeInStall(6,1)-TimeInStall(5,1) .73],'faceColor','w')
    rectangle('Position',[TimeInStall(5,1)-TimeInStall(1,1) -j-.37 270-(TimeInStall(5,1)-TimeInStall(1,1)) .73],'faceColor','k')
j=10;
    rectangle('Position',[TimeInStall(5,2)-TimeInStall(1,1) -j-.37 TimeInStall(6,2)-TimeInStall(5,2) .73],'faceColor','w')
    rectangle('Position',[TimeInStall(5,1)-TimeInStall(1,1) -j-.37 332-(TimeInStall(5,1)-TimeInStall(1,1)) .73],'faceColor','k')
    
j=11;
    rectangle('Position',[TimeInStall(6,1)-TimeInStall(1,1) -j-.37 TimeEnd-TimeInStall(6,1) .73],'faceColor','k')
j=12;
    rectangle('Position',[TimeInStall(6,2)-TimeInStall(1,1) -j-.37 TimeEnd-TimeInStall(6,2) .73],'faceColor','w')
    rectangle('Position',[TimeInStall(6,2)-TimeInStall(1,1) -j-.37 365-(TimeInStall(6,2)-TimeInStall(1,1)) .73],'faceColor','k')
    
    
disp(['Creating figure ' ])
    
if exist ('FigPrep','file'),	FigPrep;	end
if exist ('AxisPrep','file'),	AxisPrep;	end
    
%% Hard-coded figure tuning
Tlim	=	[	0	750                 ]   ;
Ttick	=	[	Tlim(1):60:Tlim(end)	]   ;
typenames={'T1a','T1b','T2a','T2b','T3a','T3b','T4a','T4b',...
            'T5a','T5b','T5c','T5d'}                    ;    
% datetick('x','yyyy.mm.dd','keeplimits')
set(gca,'Xlim',Tlim,'Ylim',-[12.73 .37 ])
set(gca,'Xtick',Ttick,'Ytick',-[12:-1:1],'Yticklabel',fliplr(typenames))
xlabel('Time [d]')
grid on

filepath        =	[	dir_figures	'figure_scheme'	]       ;

if ~InOctave
  saveas (gcf,filepath,'fig')
  saveas (gcf,filepath,'epsc')
  saveas (gcf,filepath,'tiff')
end